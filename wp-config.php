<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', 'estate_fiorentina' );

/** Nome utente del database MySQL */
define( 'DB_USER', 'root' );

/** Password del database MySQL */
define( 'DB_PASSWORD', 'root' );

/** Hostname MySQL  */
define( 'DB_HOST', 'localhost' );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'z/z?MX(V|n|HJ_H !ec=p=EgmSO]u;RwI9,CEw#frq* a;-$W=#X?YZn&1f{?$MY' );
define( 'SECURE_AUTH_KEY',  'q~F81buRb5.JvRo=xj=,?!EP` Zp%3<y{(*I3X]kI|rXlM]*BU$%C).T 1bH #$.' );
define( 'LOGGED_IN_KEY',    'A1D0x.bns^5.NqBa==+F0im3H2V_)]$|!u]/oswR4^vCHuG*.VKa(TEjbLOLiY!)' );
define( 'NONCE_KEY',        'Cs%tFVNM|FQk,kIi0;09:7|d9(fv.$r|*xw14DsJ.O???]x~akvDz^?{3E/ojIrD' );
define( 'AUTH_SALT',        'Q_k^eSkCV~,E5q~dolayV}5>qk hCrUn_55Qe_&sSx=J&q/?`QH.Rw)}5OB3<,V)' );
define( 'SECURE_AUTH_SALT', '7!Xgmn;R:}v_}GSeJ=)QJHC%$^~v$Vt(R=$|ErXAH,#xRE({yBK?HDF[teHDZ]nv' );
define( 'LOGGED_IN_SALT',   'C+K27KdJgh/Zn,bQLV7?/F2y]{D;&WQ>[O%7P!fRGyFxhV5=AOsIDRkT^stp~yCK' );
define( 'NONCE_SALT',       '$LUynibV7|i)hXlP]s`UuW}`?Mk4//~H}) wsRWab/U<e2,F0:R.yM|?auf sKh_' );

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'ef_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', true);

/* Finito, interrompere le modifiche! Buon creazione di contenuti. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
