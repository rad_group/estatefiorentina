<?php
    
    /* 
    * Helper function to return the theme option value. If no value has been saved, it returns $of_default.
    * Needed because options are saved as serialized strings.
    */

    // Options Framework Plugin Code

    if ( !function_exists( 'of_get_option' ) )
    {
        function of_get_option($of_name, $of_default = false)
        {
            
            $optionsframework_settings = get_option('optionsframework');
            
            // Gets the unique option id
            $of_option_name = $optionsframework_settings['id'];
            
            if ( get_option($of_option_name) )
            {
                $of_options = get_option($of_option_name);
            }
                
            if ( isset($of_options[$of_name]) )
            {
                return $of_options[$of_name];
            }
            else
            {
                return $of_default;
            }
        }
    }

    // Making theme translation ready

    add_action('after_setup_theme', 'huzi_setup');

    function huzi_setup(){
        load_theme_textdomain('huzi', get_template_directory() . '/languages');
    }

    // Title Tag Support

    add_theme_support('title-tag');

    // Declare WP Menus

    function huzi_custom_menus() {
        register_nav_menus( array (
            'main_nav' => esc_html__('Main Navigation Menu', 'huzi'),
            'footer_nav' => esc_html__('Footer Navigation Menu', 'huzi')
        ) );
    }
    add_action( 'init', 'huzi_custom_menus' );

    // Adding post thumbnail support

    if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
   
    if ( function_exists( 'add_image_size' ) )
    {   
        add_image_size( 'huzi-large-feat-thumb', 598, 550, true );
        add_image_size( 'huzi-medium-feat-thumb', 598, 273, true );
        add_image_size( 'huzi-small-feat-thumb', 297, 273, true );
        add_image_size( 'huzi-mega-menu-thumb', 262, 170, true );
        add_image_size( 'huzi-large-post-thumb', 786, 436, true );
        add_image_size( 'huzi-medium-post-thumb', 372, 250, true );
        add_image_size( 'huzi-small-post-thumb', 150, 105, true );
        add_image_size( 'huzi-tall-post-thumb', 372, 510, true );
        add_image_size( 'huzi-carousel-thumb', 398, 270, true );
        add_image_size( 'huzi-slider-thumb', 786, 551, true );
        add_image_size( 'huzi-feat-carousel-thumb', 598, 551, true );
        add_image_size( 'huzi-full-width-thumb', 1200, 520, true );
    }

    // Load Google Fonts

    if ( !function_exists( 'huzi_fonts_url' ) ) :
    
        function huzi_fonts_url() {
            $fonts_url = '';
            $fonts     = array();
            $subsets   = '';

            /* 
            * Loading Google Fonts depending on the selected theme design in the admin options panel.
            *
            * Translators: If there are characters in your language that are not supported by these fonts, translate these 
            * to 'off'. Do not translate into your own language.
            */

            if ( 'off' !== esc_html_x( 'on', 'Poppins font: on or off', 'huzi' ) ) {
                $fonts[] = 'Poppins:400,400i,700';
            }

            if ( $fonts ) {
                $fonts_url = add_query_arg( array(
                    'family' => urlencode( implode( '|', $fonts ) ),
                    'subset' => urlencode( $subsets ),
                ), 'https://fonts.googleapis.com/css' );
            }

            return esc_url_raw( $fonts_url );
        }

    endif;

    function huzi_fonts_enqueue() {
        wp_enqueue_style( 'huzi-fonts', huzi_fonts_url(), array(), null ); // Add custom fonts.
    }
    add_action( 'wp_enqueue_scripts', 'huzi_fonts_enqueue' );

    // Load CSS files

    function huzi_styles_load ()  
    {
        if ( !is_admin() )
        {

            wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/fonts/css/font-awesome.min.css', '', '4.7.0');

            wp_enqueue_style('huzi-style', get_stylesheet_directory_uri() . '/style.css', '', '1.0.2');

            wp_enqueue_style('huzi-media-queries', get_template_directory_uri() . '/css/media-queries.css', '', '1');
            
        }   
    }  
    add_action( 'wp_enqueue_scripts', 'huzi_styles_load', '20' );

    // Load javascript files

    function huzi_scripts_load ()
    {
        if ( !is_admin() )
        {

            if ( is_singular() ) wp_enqueue_script('comment-reply');

            wp_enqueue_script('jquery-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array('jquery'), '2.6.4', true);

            wp_enqueue_script('jquery-sticky-kit', get_template_directory_uri() . '/js/jquery.sticky-kit.min.js', array('jquery'), '1.1.2', true);

            wp_enqueue_script('jquery-masonry');
            
            wp_enqueue_script('huzi-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.2', true);
            
        }
    }
    add_action('wp_enqueue_scripts', 'huzi_scripts_load');

    /* 
    * Loading options from the options panel. This is not a css file it's a php file, this is the simplest way to add 
    * it and make sure that WordPress functions work in it.
    */

    function huzi_custom_css() 
    {
        get_template_part('custom', 'styles'); // This is a php file.
    }
    add_action('wp_head', 'huzi_custom_css');

    // Content width

    if ( ! isset( $content_width ) ) $content_width = 786;

    // Enable custom post formats

    add_theme_support('post-formats', array('video', 'audio', 'gallery'));

    // Theme Support

    add_theme_support('automatic-feed-links');

    // Declare widget zones

    function huzi_widgets_init() {

        register_sidebar(array(
            'name' => esc_html__('Home Top Sidebar Widgets', 'huzi'),
            'id'   => 'home-top-sidebar-widgets',
            'description'   => esc_html__('These are widgets for the homepage template top sidebar.', 'huzi'),
            'before_widget' => '<div id="%1$s" class="widget %2$s group">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="section-header"><h6>',
            'after_title'   => '</h6></div>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Home Bottom Sidebar Widgets', 'huzi'),
            'id'   => 'home-bottom-sidebar-widgets',
            'description'   => esc_html__('These are widgets for the homepage template bottom sidebar.', 'huzi'),
            'before_widget' => '<div id="%1$s" class="widget %2$s group">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="section-header"><h6>',
            'after_title'   => '</h6></div>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Main Sidebar Widgets', 'huzi'),
            'id'   => 'sidebar-widgets',
            'description'   => esc_html__('These are widgets for the sidebar displayed on all other pages, homepage template excluded.', 'huzi'),
            'before_widget' => '<div id="%1$s" class="widget %2$s group">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="section-header"><h6>',
            'after_title'   => '</h6></div>'
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Widgets', 'huzi'),
            'id'   => 'footer-widgets',
            'description'   => esc_html__('These are widgets for the footer.', 'huzi'),
            'before_widget' => '<div id="%1$s" class="widget %2$s group">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="section-header"><h6>',
            'after_title'   => '</h6></div>'
        ));

    }
    add_action( 'widgets_init', 'huzi_widgets_init' );

    // Comments callback function

    function huzi_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div class="comment-body group" id="comment-<?php comment_ID(); ?>">

            <div class="avatar-wrap"> 
                <?php echo get_avatar($comment,$size='60',$default='' ); ?>
            </div><!--.avatar-wrap-->

            <div class="comment-contents group">
                
                <ul class="comment-meta">
                    <li class="comment-author"><?php printf('%s', get_comment_author_link()) ?></li>
                    <li class="comment-date"><span><?php esc_html_e('On:', 'huzi'); ?></span><?php printf(' %1$s', get_comment_date()) ?></li>
                </ul>

                <div class="comment-text">
               
                    <?php comment_text() ?>
                
                    <?php if ($comment->comment_approved == '0') : ?>
                         <em class="awaiting-mod-txt"><?php esc_html_e('Your comment is awaiting moderation.', 'huzi'); ?></em>
                    <?php endif; ?>

                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
               
                </div>

            </div>
         
        </div>
<?php
    }

    include( get_template_directory() . '/lib/plugins/functions.php' );