<?php
/*
Overrides Default Styles With Custom Styles From The Themes Options Panel
*/

	echo ' <style type="text/css"> ';
	
			if ( esc_html( of_get_option('huzi_primary_color') ) ) { 
				echo '

					a,
					.entry a:hover,
					.comment-text a:hover,
					blockquote::before,
					#main-nav a:hover,
					#main-nav 
					.current-menu-item > a,
					#main-nav ul li:hover > a,
					#header-elements i:hover,
					#main-header 
					.social-share-icons i:hover,
					.post-share-txt::before,
					.post-share a:hover,
					.post-comments i,
					.tall-post 
					.post-share-txt:hover::before,
					.large-post 
					.post-share-txt:hover::before,
					#carousel-nav a:hover,
					.widget .menu li a:hover,
					.widget_pages li a:hover,
					.widget_recent_entries li a:hover,
					.widget_recent_comments li a:hover,
					.widget_archive li a:hover,
					.widget_categories li a:hover,
					.widget_meta li a:hover,
					.single-layout1 
					.single-header 
					.post-share-txt:hover::before,
					.single-layout1 
					.single-header 
					.post-share-txt:hover::before,
					#author-name a:hover,
					#author-icons a:hover,
					.comment-author a:hover,
					#slider-nav 
					.flex-next:hover::before,
					#slider-nav 
					.flex-prev:hover::before,
					.single-layout3 
					.single-header 
					.post-share-txt:hover::before,
					.go-to-home:hover,
					#menu-icon:hover,
					blockquote p::before {
						color: '.esc_html( of_get_option('huzi_primary_color') ).';
					}

					#searchsubmit,
					.video-post-icon,
					.audio-post-icon,
					.gallery-post-icon,
					.feat-post,
					#featured-tag,
					.section-header::before,
					.tall-post 
					.share-button,
					.post-rating,
					.large-post 
					.share-button,
					#carousel-posts 
					.slides > li,
					.format-video 
					.post-thumb > a::before,
					.format-gallery 
					.post-thumb > a::before,
					.format-audio 
					.post-thumb > a::before,
					.load-more:hover,
					.pagination a,
					.tagcloud a,
					#main-footer 
					.tagcloud a:hover,
					.widget #searchsubmit,
					.about-widget-social a:hover,
					.mc4wp-form 
					input[type=submit]:hover,
					.single-layout1 
					.single-header 
					.share-button,
					.single-layout3 
					.single-header 
					.share-button,
					#submit,
					#featured-slider 
					.slides > li,
					#featured-carousel .slides > li,
					#back-to-top,
					.wpcf7-submit,
					.error-wrap #searchsubmit,
					.post-password-form 
					input[type=submit] {
						background-color: '.esc_html( of_get_option('huzi_primary_color') ).';
					}

					.load-more:hover,
					.pagination a {
						border-color: '.esc_html( of_get_option('huzi_primary_color') ).';
					}

					#main-nav 
					.current-menu-item > a {
						border-bottom-color: '.esc_html( of_get_option('huzi_primary_color') ).';
					}

					#main-nav 
					.current-menu-item > a::after,
					#main-nav ul li:hover > a::after,
					#main-nav .menu-item-has-children 
					> a:hover::after {
						border-top-color: '.esc_html( of_get_option('huzi_primary_color') ).';
					}

					#main-nav ul ul 
					.menu-item-has-children > 
					a:hover::after,
					#main-nav ul ul 
					li:hover a::after,
					#main-nav ul ul 
					.current-menu-item > a::after {
						border-left-color: '.esc_html( of_get_option('huzi_primary_color') ).';
					}

				';
			}

			if ( esc_html( of_get_option('huzi_secondary_color') ) ) { 
				echo '

					#searchsubmit,
					.video-post-icon,
					.audio-post-icon,
					.gallery-post-icon,
					#featured-tag,
					.post-rating,
					.format-video 
					.post-thumb > a::before,
					.format-gallery 
					.post-thumb > a::before,
					.format-audio 
					.post-thumb > a::before,
					.load-more:hover,
					.pagination a,
					.tagcloud a,
					#main-footer 
					.tagcloud a:hover,
					.widget #searchsubmit,
					.about-widget-social a:hover,
					.mc4wp-form 
					input[type=submit]:hover,
					#submit,
					#back-to-top,
					.large-post .post-share-txt,
					.large-post .post-share-txt::before,
					.tall-post .post-share-txt,
					.tall-post .post-share-txt::before,
					.single-layout1 
					.single-header 
					.post-share-txt,
					.single-layout1 
					.single-header 
					.post-share-txt::before,
					.single-layout3 
					.single-header 
					.post-share-txt,
					.single-layout3 
					.single-header 
					.post-share-txt::before,
					.wpcf7-submit,
					.error-wrap #searchsubmit,
					.post-password-form 
					input[type=submit] {
						color: '.esc_html( of_get_option('huzi_secondary_color') ).';
					}

				';
			}

			if ( esc_html( of_get_option('huzi_sticky_header_option') ) == 'off' ) {
				echo '

					#main-header.stick-it {
						position: static;
					}

				';
			}

	echo ' </style> ';