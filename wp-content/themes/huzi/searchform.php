<form action="<?php echo esc_url( home_url( '/' ) ); ?>" id="searchform" method="get">
    <input type="text" id="s" name="s" value="<?php esc_attr_e('Enter search here ...', 'huzi'); ?>">
    <input type="submit" value="<?php esc_attr_e('Search', 'huzi'); ?>" id="searchsubmit">
</form>