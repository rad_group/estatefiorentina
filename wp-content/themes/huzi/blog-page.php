<?php
	/*
		Template Name: Blog Page
	*/
?>

<?php get_header(); ?>

<div class="wrapper clear-both group">

	<div class="posts-wrap recent-posts">

		<div id="medium-posts">

			<?php

				if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
				elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
				else { $paged = 1; }

				$huzi_number_of_posts = get_option('posts_per_page');

				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query();
				$wp_query->query('posts_per_page='.$huzi_number_of_posts.'&paged='.$paged);

				while ($wp_query->have_posts()) : $wp_query->the_post();

			?>

				<?php get_template_part('template-parts/posts/medium', 'post'); ?>

			<?php endwhile; ?>

		</div><!--#medium-posts-->

		<?php get_template_part('template-parts/pagination'); ?>

		<?php wp_reset_postdata(); ?>

	</div><!--.posts-wrap-->

	<div class="sidebar-wrap main-sidebar-wrap">

		<?php get_sidebar(); ?>

	</div><!--.sidebar-wrap-->

</div><!--.wrapper-->

<?php get_footer(); ?>