<?php get_header(); ?>

	<div class="wrapper clear-both group">

		<div class="posts-wrap page-temp-wrap">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article <?php post_class('page-temp-article group'); ?> >

					<?php if ( has_post_thumbnail() ) { ?>

						<div class="post-thumb single-thumb">

							<?php the_post_thumbnail( 'huzi-single-thumb', array( 'alt' => esc_attr( get_the_title() ) ) ); ?>

						</div>

					<?php } ?>

					<div class="entry">

						<h2 class="page-title"><?php the_title(); ?></h2>

						<?php the_content(); ?>

					</div><!--.entry-->

				</article>

				<?php comments_template(); ?>

			<?php endwhile; endif; ?>

			<?php wp_link_pages(); ?>

		</div><!--.posts-wrap-->

		<div class="sidebar-wrap main-sidebar-wrap">

			<?php get_sidebar(); ?>

		</div><!--.sidebar-wrap-->

	</div><!--.wrapper-->

<?php get_footer(); ?>