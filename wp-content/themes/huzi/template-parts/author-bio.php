<?php 

	// Template part for displaying author bio 

	$huzi_description = esc_html( get_the_author_meta( 'description' ) );
	if( $huzi_description != null ) {
		
?>

<div id="single-elements" class="group">

	<div id="about-author">

		<div id="author-avatar">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), '80' ); ?>
		</div><!--#author-avatar-->
		
		<div id="author-text">

			<h6 id="author-name"><a href="<?php echo esc_url( get_the_author_meta( 'user_url' ) ); ?>"><?php the_author(); ?></a></h6>
	 
			<p><?php echo esc_html( get_the_author_meta( 'description' ) ); ?></p>

			<ul id="author-icons">

				<?php if ( esc_url( get_the_author_meta( 'twitter' ) ) != '' )  { ?>	
					<li><a class="twitter-link" title="<?php esc_html_e('Twitter', 'huzi'); ?>" href="<?php echo esc_url( get_the_author_meta( 'twitter' ) ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<?php } ?>

				<?php if ( esc_url( get_the_author_meta( 'facebook' ) ) != '' )  { ?>	
					<li><a class="facebook-link" title="<?php esc_html_e('Facebook', 'huzi'); ?>" href="<?php echo esc_url( get_the_author_meta( 'facebook' ) ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<?php } ?>

				<?php if ( esc_url( get_the_author_meta( 'google' ) ) != '' )  { ?>	
					<li><a class="google-link" title="<?php esc_html_e('Google+', 'huzi'); ?>" href="<?php echo esc_url( get_the_author_meta( 'google' ) ); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<?php } ?>

				<?php if ( esc_url( get_the_author_meta( 'instagram' ) ) != '' )  { ?>	
					<li><a class="instagram-link" title="<?php esc_html_e('Instagram', 'huzi'); ?>" href="<?php echo esc_url( get_the_author_meta( 'instagram' ) ); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<?php } ?>

				<?php if ( esc_url( get_the_author_meta( 'linkedin' ) ) != '' )  { ?>	
					<li><a class="linkedin-link" title="<?php esc_html_e('LinkedIn', 'huzi'); ?>" href="<?php echo esc_url( get_the_author_meta( 'instagram' ) ); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				<?php } ?>
			
			</ul>
				
		</div><!--#author-text-->
		
	</div><!--#about-author-->

</div><!--#single-elements-->

<?php

	}

?>