<?php
/**
 * Single Post Layout 2 template part.
 */
?>

<div class="posts-wrap group single-layout2 single-wrap">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php

			$huzi_num_comments = get_comments_number();

			if ( comments_open() ) {

				if ( $huzi_num_comments == 0 ) {

					$huzi_comments = esc_html__('0 Comments', 'huzi');

				} elseif ( $huzi_num_comments > 1 ) {

					$huzi_comments = $huzi_num_comments . esc_html__(' Comments', 'huzi');

				} else {

					$huzi_comments = esc_html__('1 Comment', 'huzi');
				}

			} else {

				$huzi_comments =  esc_html__('Comments closed', 'huzi');

			}

		?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('single-wrap group'); ?> >

			<div class="single-header group">

				<h1 class="single-post-title post-title"><?php the_title(); ?></h1>

				<ul class="single-meta post-meta group">
					<li class="post-date"><span><?php esc_html_e('On: ', 'huzi'); ?></span><?php the_time( get_option('date_format') ); ?></li>
					<li><span><?php esc_html_e('In: ', 'huzi'); ?></span><?php the_category(', '); ?></li>
					<li class="post-author"><span><?php esc_html_e('By: ', 'huzi'); ?></span><?php the_author_posts_link(); ?></li>
					<li class="post-comments"><i class="fa fa-comments-o" aria-hidden="true"></i><?php echo esc_html( $huzi_comments ); ?></li>
				</ul>

				<?php
					if ( shortcode_exists( 'huzi-single-share' ) ) {
						echo huzi_single_share();
					}
				?>

			</div><!--.single-header-->

			<?php if ( has_post_thumbnail() ) { ?>

				<div class="post-thumb single-thumb">

					<?php the_post_thumbnail( 'huzi-large-post-thumb', array( 'alt' => esc_attr( get_the_title() ) ) ); ?>

				</div><!--.post-thumb-->

			<?php } ?>

			<div class="entry group">

				<?php the_content(); ?>
				<?php wp_link_pages(); ?>

			</div><!--.entry-->

		</article>

		<?php the_tags( '<div class="single-tags tagcloud group">','','</div>' ); ?>

		<?php get_template_part('template-parts/author', 'bio'); ?>

		<?php get_template_part('template-parts/posts/related', 'posts'); ?>

		<?php comments_template(); ?>

	<?php endwhile; endif; ?>

</div><!--.posts-wrap-->

<div class="sidebar-wrap">

	<?php get_sidebar(); ?>

</div><!--.sidebar-wrap-->
