<?php
/**
 * Tall Thumb Post template part.
 */
?>

<?php $huzi_meta_value = get_post_meta( get_the_ID(), 'meta-rating', true ); ?>

<?php if( !empty( $huzi_meta_value ) ) { ?>

<article <?php post_class('tall-post group rated-post'); ?>>

<?php } else { ?>

<article <?php post_class('tall-post group'); ?>>

<?php } ?>

	<?php if ( has_post_thumbnail() ) { ?>

		<div class="post-thumb">
			<a href="<?php the_permalink(); ?>">

				<?php if( !empty( $huzi_meta_value ) ) { ?>
					<span class="post-rating"><?php echo esc_html( $huzi_meta_value ); ?></span>
				<?php } ?>

				<?php the_post_thumbnail('huzi-tall-post-thumb'); ?>

			</a>
		</div><!--.post-thumb-->

	<?php } ?>

	<div class="post-content group">

		<h5 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

		<ul class="post-meta">
			<li class="post-date"><span><?php esc_html_e('On: ', 'huzi');?></span><a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format') ); ?></a></li>
		</ul>

		<?php
			if ( shortcode_exists( 'huzi-social' ) ) {
				echo huzi_social_share(); 
			}
		?>

	</div><!--.post-content-->

</article>