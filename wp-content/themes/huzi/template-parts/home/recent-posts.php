<?php
/**
 * Recent Posts template part.
 */
?>

<section id="recent-posts">

	<?php if( is_front_page() && !is_paged() ) { ?>
	
		<div class="section-header">
			<h6><?php echo esc_html( of_get_option('recent_posts_heading') ); ?></h6>
		</div>

	<?php } ?>

	<div id="medium-posts">

		<?php

			if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
			elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
			else { $paged = 1; }

			$huzi_number_of_posts = of_get_option('recent_posts');

			$temp = $wp_query;
			$wp_query= null;
			$wp_query = new WP_Query();
			$wp_query->query('posts_per_page='.$huzi_number_of_posts.'&paged='.$paged);

			while ($wp_query->have_posts()) : $wp_query->the_post();

				get_template_part('template-parts/posts/medium', 'post');

			endwhile;

		?>

	</div><!--#medium-posts-->

	<?php if( esc_html( of_get_option('posts_load_option') ) == 'loadmore' ) { ?>

		<?php get_template_part('template-parts/load', 'more'); ?>

	<?php } else { ?>

		<?php get_template_part('template-parts/pagination'); ?>

	<?php } ?>

</section>

<?php wp_reset_postdata(); ?>