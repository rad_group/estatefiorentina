<?php
/**
 * Home Top Sidebar template part.
 */
?>

<?php if ( is_active_sidebar( 'home-top-sidebar-widgets' ) ) { ?>
    <aside class="top-sidebar widget-sidebar group">

        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar(esc_html__('Home Top Sidebar Widgets', 'huzi'))) : else : ?>

        <?php endif; ?>

    </aside>
<?php } ?>
