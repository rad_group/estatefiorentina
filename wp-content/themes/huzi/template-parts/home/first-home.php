<?php
/**
 * First Home template part.
 */
?>

<?php if( is_front_page() && !is_paged() ) { ?>

	<?php get_template_part('template-parts/featured/first', 'featured'); ?>

	<?php get_template_part('template-parts/home/full-categories', 'posts'); ?>

	<div class="wrapper clear-both group">

		<div class="posts-wrap respo-wrap">

			<?php get_template_part('template-parts/home/review', 'posts'); ?>

			<?php get_template_part('template-parts/home/staff', 'picks'); ?>

		</div><!--.posts-wrap-->

		<div class="sidebar-wrap">

			<?php get_template_part('template-parts/home/home-top', 'sidebar'); ?>

		</div><!--.sidebar-wrap-->

	</div><!--.wrapper-->

	<?php get_template_part('template-parts/home/carousel', 'posts'); ?>

<?php } ?>

<div class="wrapper clear-both group">

	<div class="posts-wrap recent-posts">

		<?php get_template_part('template-parts/home/recent', 'posts'); ?>

	</div><!--.posts-wrap-->

	<div class="sidebar-wrap bottom-sidebar-wrap">

		<?php get_template_part('template-parts/home/home-bottom', 'sidebar'); ?>

	</div><!--.sidebar-wrap-->

</div><!--.wrapper-->
