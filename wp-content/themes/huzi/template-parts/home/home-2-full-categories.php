<?php
/**
 * Home 2 Full Categories Posts template part.
 */
?>

<div id="full-categories-posts">

	<section>
		<div class="section-header">
			<h6><?php echo esc_html( of_get_option('home_2_col_1_heading') ); ?></h6>
		</div>

		<?php

			global $post;

			$huzi_catergory_option = of_get_option('home_2_col_1_cat');

			$huzi_args = array(
				'numberposts' => 1,
				'cat' => $huzi_catergory_option
			);

			$huzi_custom_posts = get_posts($huzi_args);

			foreach($huzi_custom_posts as $post) : setup_postdata($post);

		?>

			<?php get_template_part('template-parts/posts/medium-no', 'excerpt'); ?>

		<?php
			endforeach;
		?>

		<?php

			global $post;

			$huzi_catergory_option = of_get_option('home_2_col_1_cat');

			$huzi_args = array(
				'numberposts' => 2,
				'offset' => 1,
				'cat' => $huzi_catergory_option
			);

			$huzi_custom_posts = get_posts($huzi_args);

			foreach($huzi_custom_posts as $post) : setup_postdata($post);

		?>

			<?php get_template_part('template-parts/posts/small', 'post'); ?>

		<?php
			endforeach;
		?>

	</section>

	<section>
		<div class="section-header">
			<h6><?php echo esc_html( of_get_option('home_2_col_2_heading') ); ?></h6>
		</div>

		<?php

			global $post;

			$huzi_catergory_option = of_get_option('home_2_col_2_cat');

			$huzi_args = array(
				'numberposts' => 1,
				'cat' => $huzi_catergory_option
			);

			$huzi_custom_posts = get_posts($huzi_args);

			foreach($huzi_custom_posts as $post) : setup_postdata($post);

		?>

			<?php get_template_part('template-parts/posts/medium-no', 'excerpt'); ?>

		<?php
			endforeach;
		?>

		<?php

			global $post;

			$huzi_catergory_option = of_get_option('home_2_col_2_cat');

			$huzi_args = array(
				'numberposts' => 2,
				'offset' => 1,
				'cat' => $huzi_catergory_option
			);

			$huzi_custom_posts = get_posts($huzi_args);

			foreach($huzi_custom_posts as $post) : setup_postdata($post);

		?>

			<?php get_template_part('template-parts/posts/small', 'post'); ?>

		<?php
			endforeach;
		?>

	</section>

	<?php wp_reset_postdata(); ?>

</div><!--#full-categories-posts-->
