<?php
/**
 * Review Posts template part.
 */
?>

<section id="review-posts">
	
	<div class="section-header">
		<h6><?php echo esc_html( of_get_option('reviews_block_heading') ); ?></h6>
	</div>

	<?php

		global $post;

		$huzi_catergory_option = of_get_option('reviews_block_cat');

		$huzi_args = array(
			'numberposts' => 2,
			'cat' => $huzi_catergory_option
		);
		
		$huzi_custom_posts = get_posts($huzi_args);

		foreach($huzi_custom_posts as $post) : setup_postdata($post);

	?>

		<?php get_template_part('template-parts/posts/tall-thumb', 'post'); ?>

	<?php
		endforeach;
	?>

</section>

<?php wp_reset_postdata(); ?>