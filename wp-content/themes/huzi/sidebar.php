<?php if ( is_active_sidebar( 'sidebar-widgets' ) ) { ?>
    <aside class="main-sidebar widget-sidebar group">

        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar(esc_html__('Main Sidebar Widgets', 'huzi'))) : else : ?>

        <?php endif; ?>

    </aside>
<?php } ?>
