<?php get_header(); ?>

<div class="wrapper clear-both group">

	<div class="posts-wrap group recent-posts">

		<?php if (have_posts()) : ?>

			<div id="medium-posts">

				<?php while (have_posts()) : the_post(); ?>

					<?php get_template_part('template-parts/posts/medium', 'post'); ?>

				<?php endwhile; ?>

			</div><!--#medium-posts-->

		<?php else : ?>

			<div class="nothing-found group">
				<h1><?php esc_html_e('Nothing Found', 'huzi'); ?></h1>
				<p><?php esc_html_e('We could not find what you are looking for. Please try again.', 'huzi'); ?></p>

				<?php the_widget( 'WP_Widget_Recent_Posts', array( 'number' => 14 ), array( 'widget_id' => 'nothing-found' ) ); ?>
			</div>

		<?php endif; ?>

		<?php get_template_part('template-parts/pagination'); ?>

	</div><!--.posts-wrap-->

	<div class="sidebar-wrap main-sidebar-wrap">

		<?php get_sidebar(); ?>

	</div><!--.sidebar-wrap-->

</div><!--.wrapper-->

<?php get_footer(); ?>