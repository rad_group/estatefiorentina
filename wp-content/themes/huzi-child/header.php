<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>

		<?php if( of_get_option('huzi_favicon_uploader') ) { ?>
			<link rel="shortcut icon" href="<?php echo esc_url( of_get_option('huzi_favicon_uploader') ); ?>">
		<?php } ?>

	<?php } ?>

	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.theme.default.min.css">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

  <div class="bg-header">
    <div class="bg-header__stripe"></div>
  </div>

	<?php if( of_get_option('huzi_header_ad_option') == 'on' ) { ?>

		<div class="banner-ad-wrapper">

			<?php if( of_get_option('huzi_header_ad') ) { ?>

				<div class="header-banner-ad">

					<a href="<?php echo esc_url( of_get_option('huzi_header_ad_url') ); ?>" class="banner-ad-url"><img alt="<?php esc_attr_e( 'banner', 'huzi' ) ?>" src="<?php echo esc_url( of_get_option('huzi_header_ad') ); ?>"></a>

				</div><!--.header-banner-ad-->

			<?php } else { ?>

				<div class="header-banner-ad">

					<!-- Paste Google Ads Code Here -->

				</div>

			<?php } ?>

		</div><!--.banner-ad-wrapper-->

	<?php } ?>

	<header id="main-header">

		<div class="wrapper">

			<?php if( esc_url( of_get_option('huzi_logo_uploader') ) ) { ?>

				<div id="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo-img"><img alt="<?php bloginfo(); ?>" src="<?php echo esc_url( of_get_option('huzi_logo_uploader') ); ?>"></a>
				</div>

			<?php } else { ?>

				<div id="text-logo">
					<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo(); ?></a></h1>
					<span id="tagline"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" ><?php bloginfo( 'description' ); ?></a></span>
				</div>

			<?php } ?>

			<?php if ( has_nav_menu( 'main_nav' ) ) { ?>

				<nav id="main-nav">

					<?php wp_nav_menu(array('menu' => esc_html__('Main Navigation Menu', 'huzi'), 'theme_location' => 'main_nav')); ?>

				</nav>

			<?php } ?>

			<?php if( of_get_option('huzi_header_icons') == 'on' ) { ?>

				<div id="header-elements">


          <span class="header-main-sponsor firenze">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/citta_di_firenze.png" alt="Logo Comune di Firenze">
          </span>


					<?php if ( shortcode_exists( 'huzi-header-share' ) ) { ?>

						<?php echo huzi_header_share(); ?>

						<i class="fa fa-share-alt" title="<?php esc_attr_e( 'Share', 'huzi' ) ?>"></i>

					<?php } ?>

					<i class="fa fa-search" title="<?php esc_attr_e( 'Search', 'huzi' ) ?>"></i>
					<i class="fa fa-close" title="<?php esc_attr_e( 'Close search', 'huzi' ) ?>"></i>

				</div><!--#header-elements-->

				<?php get_search_form(); ?>

			<?php } ?>

			<div id="menu-icon">
				<i class="fa fa-align-justify" aria-hidden="true"></i><span><?php esc_html_e('Menu', 'huzi'); ?></span>
			</div>

		</div><!--.wrapper-->

	</header>
