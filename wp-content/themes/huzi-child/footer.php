
	<?php if ( is_active_sidebar( 'footer-widgets' ) ) { ?>

		<footer id="main-footer" class="group">
			<div class="wrapper">

				<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar( esc_html__('Footer Widgets', 'huzi') ) ) : else : ?>

				<?php endif; ?>

			</div><!--.wrapper-->
		</footer>

	<?php } ?>

	<div id="copyright-wrap" class="group">

		<div id="copyright">
			<div class="wrapper">

				<p>© Estate Fiorentina <?php echo date('Y'); ?>. Tutti i diritti riservati. Sito web by <a class="credits" href="https://we-rad.com" title="WE RAD - Agenzia di Comunicazione Firenze" target="_blank">We Rad</a>.</p>

				<?php
					if ( has_nav_menu( 'footer_nav' ) ) {

						wp_nav_menu(array('menu' => esc_html__('Footer Navigation Menu', 'huzi'), 'theme_location' => 'footer_nav'));

					}
				?>

			</div><!--.wrapper-->
		</div><!--#copyright-->

	</div><!--#copyright-wrap-->

	<div id="back-to-top"></div>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js"></script>

	<?php wp_footer(); ?>

</body>

</html>
