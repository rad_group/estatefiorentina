;(function ($) {

  $(document).ready(function(){

    $('.feat-post-carousel').flexslider({
			animation: 'fade',
			animationLoop: true,
			controlNav: true,
			move: 1,
      slideshow: true,
		});

    $('.sidebar-latest-events').flexslider({
      animation: 'slide',
      animationLoop: true,
      slideshow: true,
      slideshowSpeed: 5000,
      smoothHeight: true,
      controlNav: false,
      directionNav:	true,
      prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
      direction: 'horizontal',
      move: 1,
    });


    // AutoHeight.Defaults = {
    //   autoHeight: false,
    //   autoHeightClass: 'owl-height'
    // };

    // $('.owl-carousel').owlCarousel({
    // loop:true,
    // nav:true,
    // items: 1,
    // autoHeight: true,

  });


}(jQuery));
