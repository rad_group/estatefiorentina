<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_script( 'rad-script', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ), null, true );
}

// Remove prefix from archive title
add_filter( 'get_the_archive_title', function ($title) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
    $title = single_tag_title( '', false );
  } elseif ( is_author() ) {
    $title = '<span class="vcard">' . get_the_author() . '</span>' ;
  }
  return $title;
});

function sb_add_cpts_to_api( $args, $post_type ) {
    if ( 'mec-events' === $post_type ) {
        $args['show_in_rest'] = true;
        //$args['public'] = true;
    }
    return $args;
}
add_filter( 'register_post_type_args', 'sb_add_cpts_to_api', 10, 2 );
<<<<<<< HEAD

register_rest_field( 'mec-events', 'metadata', array(
    'get_callback' => function ( $data ) {
        return get_post_meta( $data['id'], '', '' );
    }, ));
=======
>>>>>>> c043cb83f0000f941ebafa33fe9a47f28a685787
