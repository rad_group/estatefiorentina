<?php
/**
 * Full Categories Posts template part.
 */
?>

<div id="full-categories-posts">

  <?php

  $args = array(
    'exclude' => array(2,5,7,16,11,3,18),
    'parent' => '0'
  );

  // Richiamiamo tutte le categorie del post_type POST (escludendone alcune, vedi $args)
  $categories = get_terms('category', $args);?>

  <?php
  $cat_loop = array();

  // Inserimao tutte le categorie in un nuovo array $cat_loop
  foreach( $categories as $category ) {
    $args = array(
            'numberposts' => 3,
            'category'    => $category->term_id
    );

    $recent_posts = wp_get_recent_posts($args);
    $cat_list[]=array(
                'id' => $category->term_id,
                'name' => $category->name,
                'post_date' => $recent_posts[0]['post_date']
            );

    //$cat_loop[] = $category->term_id;
  }

  /* sort $cat_list on basis of resent publish post */
  // Per ogni array si richiamano 3 post, di cui, solo il primo è grosso
  function sortFunction( $a, $b ){
    return strtotime($a["post_date"]) - strtotime($b["post_date"]) > 1 ? -1 : 1;
  }
  usort($cat_list, "sortFunction");

  /* print list of sorted categories */
  echo'<ul class="cat-list">';
  foreach ($cat_list as $cat): ?>

    <section>
      <div class="section-header">
        <h6><?php echo $cat['name']; ?></h6>
      </div>

      <?php global $post; ?>

      <?php
      $huzi_args = array(
        'numberposts' => 3,
        'cat' => $cat
      );

      $huzi_custom_posts = get_posts($huzi_args);

      // Inizializiamo il conteggio per riconoscere a quale post siamo
      $i = 0;

      foreach($huzi_custom_posts as $post) : setup_postdata($post); ?>

        <?php
        // Solo il primo post ha il template-parts con lo stile grosso
        if ( $i == 0 ) {
          get_template_part('template-parts/posts/medium-no', 'excerpt');
        } else {
          // Gli altri sono piccoli
          get_template_part('template-parts/posts/small', 'post');
        } ?>

        <?php $i++; ?>

      <?php endforeach; ?>
    </section>
  <?php endforeach;
  echo '</ul>'; ?>

	<?php wp_reset_postdata(); ?>

</div><!--.wrapper-->
