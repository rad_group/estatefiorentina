<?php
/**
 * First Home template part.
 */
?>

<?php if( is_front_page() && !is_paged() ) { ?>

	<?php get_template_part('template-parts/featured/first', 'featured'); ?>

	<div class="main-post-wrapper wrapper clear-both group">

		<div class="posts-wrap respo-wrap">

			<?php get_template_part('template-parts/home/full-categories', 'posts'); ?>

		</div><!--.posts-wrap-->

		<div class="sidebar-wrap">

			<?php get_template_part('template-parts/home/home-top', 'sidebar'); ?>

		</div><!--.sidebar-wrap-->

	</div><!--.wrapper-->

	<?php get_template_part('template-parts/home/carousel', 'posts'); ?>

<?php } ?>
