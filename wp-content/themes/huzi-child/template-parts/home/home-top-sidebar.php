<?php
/**
 * Home Top Sidebar template part.
 */
?>

<?php if ( is_active_sidebar( 'home-top-sidebar-widgets' ) ) { ?>
    <aside class="top-sidebar widget-sidebar group">

      <?php
      $current_date = date('Y-m-d');

      $args = array( 'post_type' => 'mec-events', 'posts_per_page' => -1,
                    'post_status' => array('publish'),
                    'meta_query'       => array(
                            //comparison between the inner meta fields conditionals
                            'relation'    => 'AND',
                            //meta field condition one
                            array(
                                'key'          => 'mec_start_date',
                                'value'        => $current_date,
                                'compare'      => '<=',
                            ),
                            //meta field condition one
                            array(
                                'key'          => 'mec_end_date',
                                'value'        => $current_date,
                                'compare'      => '>=',
                            )
                        ),
                   );
      $loop = new WP_Query( $args );

      if ($loop->have_posts()){
     ?>
      <div style="display: none;"><?php print_r($loop); ?></div>
      <div class="sidebar-latest-events">
        <div class="section-header">
          <h6>Gli eventi di oggi</h6>
        </div>
        <ul class=" sidebar-latest-events__list slides">
          <?php //echo '<h1>' . $current_date . '</h1>';
          while ( $loop->have_posts() ) : $loop->the_post();
            //$mec_meta = get_post_meta(get_the_ID());
            //$mec_data = $mec_meta['mec_start_date'][0];
            //if ( $mec_data == $current_date ) { ?>

              <li class="sidebar-latest-events__list-item">
                <a href="<?php echo get_permalink(); ?>">
                  <figure class="sidebar-latest-events__img-box">
                    <img class="sidebar-latest-events__img" src="<?php echo get_the_post_thumbnail_url('', 'medium'); ?>" alt="<?php the_title(); ?>">
                  </figure>
                  <?php if(get_field('calendar_event_title')) { ?>
                    <h3 class="sidebar-latest-events__title"><?php echo get_field('calendar_event_title'); ?></h3>
                  <?php }else { ?>
                    <h3 class="sidebar-latest-events__title"><?php the_title(); ?></h3>
                  <?php } ?>
                  <span><?php  setlocale(LC_TIME,"it_IT"); echo strftime('%d %b %Y'); ?></span>
                </a>
                <div class="sidebar-latest-events__page-link">
                  <a href="<?php echo get_permalink(); ?>">Visualizza l'evento</a>
                </div>
              </li>
            <?php //}//else{ ?>
              <!-- <li class="sidebar-latest-events__list-item">
                <h3 class="sidebar-latest-events__title">Al momento non ci sono eventi.</h3>
              </li> -->
            <?php// } ?>
          <?php endwhile; ?>
        </ul>
      </div>
      <?php }else{ ?>
        <li class="sidebar-latest-events__list-item">
          <h3 class="sidebar-latest-events__title">Al momento non ci sono eventi.</h3>
        </li>
      <?php } ?>


      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar(esc_html__('Home Top Sidebar Widgets', 'huzi'))) : else : ?>

      <?php endif; ?>

    </aside>
<?php } ?>
