<?php
/**
 * Carousel Posts template part.
 */
?>

<div id="carousel-wrap" class="wrapper clear-both">

	<div class="section-header">
		<h6><?php echo esc_html( of_get_option('carousel_heading') ); ?></h6>
	</div>

	<div id="carousel-posts">

		<ul class="slides">

			<?php

				global $post;

				$huzi_catergory_option = of_get_option('carousel_category');
				$huzi_number_of_posts = of_get_option('carousel_number');
				$huzi_order_by = of_get_option('carousel_order');

				if( $huzi_number_of_posts > 9 ) {
					$huzi_number_of_posts = 9;
				}

				$huzi_args = array(
					'numberposts' => $huzi_number_of_posts,
					'cat' => $huzi_catergory_option,
					'orderby' => $huzi_order_by
				);

				$huzi_custom_posts = get_posts($huzi_args);

				foreach($huzi_custom_posts as $post) : setup_postdata($post);

			?>

			<li <?php post_class(); ?>>

				<div class="post-thumb">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'huzi-carousel-thumb', array( 'alt' => esc_attr( get_the_title() ) ) ); ?></a>

				</div><!--.post-thumb-->

				<div class="feat-caption">

					<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

					<ul class="post-meta">
						<li class="post-date"><?php esc_html_e('', 'huzi');?><a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format') ); ?></a></li>
					</ul>

				</div><!--.feat-caption-->

				<?php
					if ( shortcode_exists( 'huzi-social' ) ) {
						echo huzi_social_share();
					}
				?>

				<a href="<?php the_permalink(); ?>" class="gradient-overlay"></a>

			</li>

			<?php
				endforeach;
			?>

			<?php wp_reset_postdata(); ?>

		</ul>

	</div><!--#carousel-posts-->

	<div id="carousel-nav"></div>

</div><!--#carousel-wrap-->
