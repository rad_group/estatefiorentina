<?php
/**
 * Related Posts template part.
 */
?>

<?php

	$huzi_single_layout = of_get_option('huzi_single_layouts');

	if ( $huzi_single_layout == 'third-single' ) {
		$huzi_posts_per_page = 3;
	} else {
		$huzi_posts_per_page = 2;
	}

	$huzi_args = array(
		'category__in' => wp_get_post_categories($post->ID),
		'post__not_in' => array($post->ID),
		'posts_per_page' => $huzi_posts_per_page,
		'ignore_sticky_posts' => 1,
		'orderby' => 'rand'
	);

	$huzi_my_query = new WP_Query($huzi_args);
	
	if( $huzi_my_query->have_posts() ) { ?>

		<div class="section-header">
			<h6><?php esc_html_e('Related Posts', 'huzi'); ?></h6>
		</div>
	
		<div id="related-posts" class="group">

			<?php while ($huzi_my_query->have_posts()) : $huzi_my_query->the_post(); ?>
	
				<?php get_template_part('template-parts/posts/small', 'post'); ?>
	
			<?php endwhile; ?>
		
		</div>
	
	<?php }
		
	wp_reset_postdata();
	
?>