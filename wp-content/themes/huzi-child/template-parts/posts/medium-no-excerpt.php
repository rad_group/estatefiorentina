<?php
/**
 * Medium Post No Excerpt template part.
 */
?>

<?php

	$huzi_num_comments = get_comments_number();

	if ( comments_open() ) {

		if ( $huzi_num_comments == 0 ) {

			$huzi_comments = esc_html__('0 Comments', 'huzi');

		} elseif ( $huzi_num_comments > 1 ) {

			$huzi_comments = $huzi_num_comments . esc_html__(' Comments', 'huzi');

		} else {

			$huzi_comments = esc_html__('1 Comment', 'huzi');
		}

	} else {

		$huzi_comments =  esc_html__('Comments closed', 'huzi');

	}

?>

<article <?php post_class('medium-post group'); ?>>

	<?php if ( has_post_thumbnail() ) { ?>

		<div class="post-thumb">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('huzi-medium-post-thumb'); ?><span class="format-icon"></span></a>
			<?php
				if ( shortcode_exists( 'huzi-social' ) ) {
					echo huzi_social_share();
				}
			?>
		</div><!--.post-thumb-->

	<?php } ?>

	<div class="post-content group">

		<h5 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

		<ul class="post-meta">
			<li class="post-date"><a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format') ); ?></a></li>
			<li class="post-comments"><i class="fa fa-comments-o" aria-hidden="true"></i><?php echo esc_html( $huzi_comments ); ?></li>
		</ul>

	</div><!--.post-content-->

</article>
