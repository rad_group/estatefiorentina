<?php
/**
 * Small Post template part.
 */
?>

<article <?php post_class('small-post group'); ?>>

	<?php if ( has_post_thumbnail() ) { ?>

		<div class="post-thumb">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('huzi-small-post-thumb'); ?></a>
			<?php
				if ( shortcode_exists( 'huzi-social' ) ) {
					echo huzi_social_share();
				}
			?>
		</div><!--.post-thumb-->

	<?php } ?>

	<div class="post-content group">

		<h6 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>

		<ul class="post-meta">
			<li class="post-date"><a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format') ); ?></a></li>
		</ul>

	</div><!--.post-content-->

</article>
