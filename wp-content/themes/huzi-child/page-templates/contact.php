<?php
/**
 * The template to display Contact Page.
 *
 * Template Name: Contact
 *
 * This is the template that displays a page wit standard sidebar.
 *
 * @package framaework
 */
 ?>

 <?php get_header(); ?>

 	<div id="full-width-page" class="wrapper group">
    <div class = "container-contact-form-intro">
      <div class = "contact-form-intro">
        <h6> Utilizza il form sottostante per richiedere tutte le informazioni riguardanti gli eventi dell'Estate Fiorentina 2018. </h6>
      </div>
    </div>

   		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

   			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
     			
     			<div class="rad-row">
       			
       			<div class="rad-row__column tab-6">
         			<div class="entry group">
           			<p>Puoi contattare lo Staff Comunicazione del sito <strong>Estate Fiorentina 2017</strong><br>
             			al seguente indirizzo email: <a href="mailto:comunicazione.ef@comune.fi.it" title="Mail Estate Fiorentina">comunicazione.ef@comune.fi.it</a></p>
         			</div>
       			</div>
       			
       			<div class="rad-row__column tab-6">
         			<div class="entry group">
       					<?php the_content(); ?>
     				  </div>
       			</div>
  
     				<?php wp_link_pages(); ?>
   				
     			</div>

   			</article>

   		<?php endwhile; endif; ?>

   	</div><!--#full-width-page-->

 <?php get_footer(); ?>
