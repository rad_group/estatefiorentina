<?php
/** no direct access **/
defined('MECEXEC') or die();

$styling = $this->main->get_styling();
$settings = $this->main->get_settings();
$current_month_divider = $this->request->getVar('current_month_divider', 0);

$event_colorskin = (isset($styling['mec_colorskin']) || isset($styling['color'])) ? 'colorskin-custom' : '';
?>
<div class="mec-wrap <?php echo $event_colorskin; ?>">
	<div class="mec-event-list-<?php echo $this->style; ?>">
		<div class="grid-container--large">
			<div class="row">
				<?php foreach($this->events as $date=>$events): ?>

					<?php $month_id = date('Ym', strtotime($date));
						if($this->month_divider and $month_id != $current_month_divider): $current_month_divider = $month_id; ?>
							<div class="row__column">
					    	<div class="mec-month-divider" data-toggle-divider="mec-toggle-<?php echo date_i18n('Ym', strtotime($date)); ?>-<?php echo $this->id; ?>">
									<span><?php echo date_i18n('F Y', strtotime($date)); ?></span>
									<i class="mec-sl-arrow-down"></i>
								</div>
							</div>
				    <?php endif; ?>

					<?php
		        foreach($events as $event) {
		          $location = isset($event->data->locations[$event->data->meta['mec_location_id']]) ? $event->data->locations[$event->data->meta['mec_location_id']] : array();
		          $organizer = isset($event->data->organizers[$event->data->meta['mec_organizer_id']]) ? $event->data->organizers[$event->data->meta['mec_organizer_id']] : array();
		          $start_time = (isset($event->data->time) ? $event->data->time['start'] : '');
		          $end_time = (isset($event->data->time) ? $event->data->time['end'] : '');
		          $event_color = isset($event->data->meta['mec_color']) ? '<span class="event-color" style="background: #'.$event->data->meta['mec_color'].'"></span>' : '';
		          $label_style = '';

		          if(!empty($event->data->labels)){
		            foreach($event->data->labels as $label){
		              if(!isset($label['style']) or (isset($label['style']) and !trim($label['style']))) continue;
		              if($label['style']  == 'mec-label-featured'){
		                $label_style = esc_html__('Featured' , 'mec');
		              } elseif($label['style']  == 'mec-label-canceled'){
		                $label_style = esc_html__('Canceled' , 'mec');
		              }
		            }
		          }
		          $speakers = '""';
		          if ( !empty($event->data->speakers)){
		            $speakers= [];
		            foreach ($event->data->speakers as $key => $value) {
		              $speakers[] = array(
		                "@type" 	=> "Person",
		                "name"		=> $value['name'],
		                "image"		=> $value['thumbnail'],
		                "sameAs"	=> $value['facebook'],
		              );
		            }
		            $speakers = json_encode($speakers);
		          }
		        ?>
		        <div class="row__column tab-6">
							<script type="application/ld+json">
			        {
			            "@context" 		: "http://schema.org",
			            "@type" 		: "Event",
			            "startDate" 	: "<?php echo !empty( $event->data->meta['mec_date']['start']['date'] ) ? $event->data->meta['mec_date']['start']['date'] : '' ; ?>",
			            "endDate" 		: "<?php echo !empty( $event->data->meta['mec_date']['end']['date'] ) ? $event->data->meta['mec_date']['end']['date'] : '' ; ?>",
			            "location" 		:
			            {
			                "@type" 		: "Place",
			                "name" 			: "<?php echo (isset($location['name']) ? $location['name'] : ''); ?>",
			                "image"			: "<?php echo (isset($location['thumbnail']) ? esc_url($location['thumbnail'] ) : '');; ?>",
			                "address"		: "<?php echo (isset($location['address']) ? $location['address'] : ''); ?>"
			            },
			            "performer": <?php echo $speakers; ?>,
			            "description" 	: "<?php  echo esc_html(preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<div class="figure">$1</div>', $event->data->post->post_content)); ?>",
			            "image" 		: "<?php echo !empty($event->data->featured_image['full']) ? esc_html($event->data->featured_image['full']) : '' ; ?>",
			            "name" 			: "<?php esc_html_e($event->data->title); ?>",
			            "url"			: "<?php echo $this->main->get_event_date_permalink($event->data->permalink, $event->date['start']['date']); ?>"
			        }
			        </script>

			        <article data-style="<?php echo $label_style; ?>" class="mec-event-article mec-clear single-articolo event <?php echo $this->get_event_classes($event); ?> mec-divider-toggle mec-toggle-<?php echo date_i18n('Ym', strtotime($date)); ?>-<?php echo $this->id; ?>">
								<?php $terms = wp_get_post_terms(get_the_ID(), 'mec_category'); ?>
								<a class="single-articolo__link" title="<?php the_title(); ?>" data-event-id="<?php echo $event->data->ID; ?>" href="<?php echo $this->main->get_event_date_permalink($event->data->permalink, $event->date['start']['date']); ?>">
									<div class="row--stuck">
										<div class="row__column tab-5">
											<div class="single-articolo__tag txt-left">
												<i class="mec-sl-calendar"></i>
												<h4>Evento</h4>
											</div>
											<div class="single-articolo__img">
												<div class="single-articolo__thumb" style="background-image: url('<?php echo get_the_post_thumbnail_url($event->data->ID) ?>')">
												</div>
											</div>
										</div>

										<div class="row__column tab-7">
							        <div class="single-articolo__wrap">
							          <div class="single-articolo__head">
							            <?php foreach ($terms as $term) : ?>
							              <div class="category-label <?php echo $term->slug; ?>" href="<?php echo get_term_link($term->term_id); ?>"> <?php echo $term->name; ?> </div>
							            <?php endforeach; ?>
							            <h2 class="single-articolo__title">
														<?php echo $event->data->title; ?>
							            </h2>
							          </div>


						            <div class="single-articolo__info">
						              <div class="single-articolo__date">

														<?php if(isset($settings['multiple_day_show_method']) && $settings['multiple_day_show_method'] == 'all_days') : ?>
															<i class="mec-sl-calendar"></i>
															<span>
																<?php echo date_i18n($this->date_format_classic_1, strtotime($event->date['start']['date'])); ?>
															</span>
														<?php else: ?>
															<i class="mec-sl-calendar"></i>
															<span>
																<?php echo $this->main->date_label($event->date['start'], $event->date['end'], $this->date_format_classic_1); ?>
															</span>
														<?php endif; ?>

						              </div>
													<?php if(isset($location['name'])): ?>
														<div class="single-articolo__place">
					                    <i class="mec-sl-location-pin"></i>
					                    <?php echo (isset($location['name']) ? $location['name'] : ''); ?>
						                </div>
													<?php endif; ?>

						            </div>
							        </div>
							      </div>

									</div>
									<?php
									/*
									<?php if(isset($settings['multiple_day_show_method']) && $settings['multiple_day_show_method'] == 'all_days') : ?>
									<div class="mec-event-date mec-color">
									<i class="mec-sl-calendar"></i> <?php echo date_i18n($this->date_format_classic_1, strtotime($event->date['start']['date'])); ?>
									</div>
									<?php else: ?>
									<div class="mec-event-date mec-color">
									<i class="mec-sl-calendar"></i> <?php echo $this->main->date_label($event->date['start'], $event->date['end'], $this->date_format_classic_1); ?>
									</div>
									<?php endif; ?>
									*/
									?>
								</a>
			      	</article>
		        </div>
		      <?php } ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
