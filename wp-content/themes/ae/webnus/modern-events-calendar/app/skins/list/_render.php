<?php
/** no direct access **/
defined('MECEXEC') or die();

$styling = $this->main->get_styling();
$settings = $this->main->get_settings();
$current_month_divider = $this->request->getVar('current_month_divider', 0);

$event_colorskin = (isset($styling['mec_colorskin']) || isset($styling['color'])) ? 'colorskin-custom' : '';
?>
<div class="mec-wrap <?php echo $event_colorskin; ?>">
	<div class="mec-event-list-<?php echo $this->style; ?>">
		<?php foreach($this->events as $date=>$events): ?>

      <?php $month_id = date('Ym', strtotime($date));
			if($this->month_divider and $month_id != $current_month_divider): $current_month_divider = $month_id; ?>
	      <div class="mec-month-divider" data-toggle-divider="mec-toggle-<?php echo date_i18n('Ym', strtotime($date)); ?>-<?php echo $this->id; ?>"><span><?php echo date_i18n('F Y', strtotime($date)); ?></span><i class="mec-sl-arrow-down"></i></div>
      <?php endif; ?>

      <?php
        foreach($events as $event){
          $location = isset($event->data->locations[$event->data->meta['mec_location_id']]) ? $event->data->locations[$event->data->meta['mec_location_id']] : array();
          $organizer = isset($event->data->organizers[$event->data->meta['mec_organizer_id']]) ? $event->data->organizers[$event->data->meta['mec_organizer_id']] : array();
          $start_time = (isset($event->data->time) ? $event->data->time['start'] : '');
          $end_time = (isset($event->data->time) ? $event->data->time['end'] : '');
          $event_color = isset($event->data->meta['mec_color']) ? '<span class="event-color" style="background: #'.$event->data->meta['mec_color'].'"></span>' : '';
          $label_style = '';

          if(!empty($event->data->labels)){
            foreach($event->data->labels as $label){
              if(!isset($label['style']) or (isset($label['style']) and !trim($label['style']))) continue;
              if($label['style']  == 'mec-label-featured'){
                $label_style = esc_html__('Featured' , 'mec');
              }elseif($label['style']  == 'mec-label-canceled'){
                $label_style = esc_html__('Canceled' , 'mec');
              }
            }
          }
          $speakers = '""';
          if ( !empty($event->data->speakers)){
            $speakers= [];
            foreach ($event->data->speakers as $key => $value) {
              $speakers[] = array(
                "@type" 	=> "Person",
                "name"		=> $value['name'],
                "image"		=> $value['thumbnail'],
                "sameAs"	=> $value['facebook'],
              );
            }
            $speakers = json_encode($speakers);
          } ?>
	        <script type="application/ld+json">
	        {
	          "@context" 		: "http://schema.org",
	          "@type" 		: "Event",
	          "startDate" 	: "<?php echo !empty( $event->data->meta['mec_date']['start']['date'] ) ? $event->data->meta['mec_date']['start']['date'] : '' ; ?>",
	          "endDate" 		: "<?php echo !empty( $event->data->meta['mec_date']['end']['date'] ) ? $event->data->meta['mec_date']['end']['date'] : '' ; ?>",
	          "location" 		:
	          {
	            "@type" 		: "Place",
	            "name" 			: "<?php echo (isset($location['name']) ? $location['name'] : ''); ?>",
	            "image"			: "<?php echo (isset($location['thumbnail']) ? esc_url($location['thumbnail'] ) : '');; ?>",
	            "address"		: "<?php echo (isset($location['address']) ? $location['address'] : ''); ?>"
	          },
	          "performer": <?php echo $speakers; ?>,
	          "description" 	: "<?php  echo esc_html(preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<div class="figure">$1</div>', $event->data->post->post_content)); ?>",
	          "image" 		: "<?php echo !empty($event->data->featured_image['full']) ? esc_html($event->data->featured_image['full']) : '' ; ?>",
	          "name" 			: "<?php esc_html_e($event->data->title); ?>",
	          "url"			: "<?php echo $this->main->get_event_date_permalink($event->data->permalink, $event->date['start']['date']); ?>"
	        }
	        </script>
					<div class="row__column desk-6">
						<?php get_template_part( 'template-parts/content', 'single-article' ); ?>
					</div>
        <?php } ?>
			<?php endforeach; ?>
  	</div>
	</div>
</div>
