<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<?php
$terms = wp_get_post_terms(get_the_ID(), 'category');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

  <div class="single-post__header-offset"></div>
  <header class="single-post__header">
    <div class="grid-container--large">
      <?php foreach ($terms as $term) : ?>
        <div class="single-post__header-category"><?php echo $term->name; ?></div>
      <?php endforeach; ?>
      <h1 class="single-post__header-title"><?php the_title(); ?></h1>
    </div>
  </header>

  <div class="grid-container--large">
    <div class="single-post__header-thumb">
      <?php echo get_the_post_thumbnail(); ?>
    </div>

    <div class="single-post__content">
      <?php the_content(); ?>
    </div><!-- .single-post__content -->
  </div>

</article><!-- #post-## -->
