<?php
if (get_post_type() == 'post') {
  $terms = wp_get_post_terms(get_the_ID(), 'category');
} elseif (get_post_type() == 'mec-events') {
  $terms = wp_get_post_terms(get_the_ID(), 'mec_category');
}

if ( has_post_thumbnail() ) {
  $post_thumb = get_the_post_thumbnail_url();
} else {
  $post_thumb = get_bloginfo('template_directory') . '/img/image-not-found.png';
}
?>

<article class="single-article <?php if (get_post_type() == 'post') { echo 'standard-post'; } else { echo 'event'; } ?>">
  <a class="single-article__link" href="<?php echo get_the_permalink(); ?>" title="<?php the_title(); ?>">
    <div class="single-article__img">
      <div class="single-article__thumb" style="background-image: url('<?php echo $post_thumb; ?>')">
      </div>
    </div>
    <div class="single-article__wrap">
      <div class="single-article__head">
        <?php foreach ($terms as $term) : ?>
          <div class="category-label <?php echo $term->slug; ?>"><?php echo $term->name; ?></div>
        <?php endforeach; ?>
        <h2 class="single-article__title">
          <?php echo get_the_title(); ?>
        </h2>
      </div>

      <?php if (get_post_type() == 'mec-events') : ?>
        <div class="single-article__info">
          <?php $getPostCustom = get_post_custom(); ?>
          <div class="single-article__date">
            <?php if ( $getPostCustom['mec_start_date'][0] ) : ?>
              <i class="mec-sl-calendar"></i>
              <span class="single-article__date--start"><?php echo $getPostCustom['mec_start_date'][0]; ?></span>
            <?php endif; ?>
            <?php if ( $getPostCustom['mec_start_date'][0] != $getPostCustom['mec_end_date'][0] ) : ?>
              <span class="single-article__date--end"> • <?php echo $getPostCustom['mec_end_date'][0]; ?></span>
            <?php endif; ?>
          </div>
          <?php if (get_post_type() == 'mec-events') : ?>
            <?php $loc_terms = wp_get_post_terms(get_the_ID(), 'mec_location'); ?>
            <div class="single-article__place">
              <?php foreach ($loc_terms as $loc_term) : ?>
                <i class="mec-sl-location-pin"></i>
                <span class="single-article__place-label">
                  <?php echo $loc_term->name; ?>
                </span>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </a>
</article>
