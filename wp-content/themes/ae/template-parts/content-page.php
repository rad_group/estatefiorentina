<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

  <?php if ( get_field('enable_big_hero') == true) : ?>
    <?php get_template_part( 'template-parts/content', 'hero' ); ?>
  <?php else : ?>
    <header class="page__header">
      <?php the_title( '<h1 class="page__title">', '</h1>' ); ?>
    </header><!-- .page__header -->
  <?php endif; ?>

  <div class="page__content">
    <?php the_content( 'template-parts/composer' ); ?>
  </div><!-- .page__content -->

</article><!-- #post-## -->
