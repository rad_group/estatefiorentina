<?php
global $wp;
$current_slug = add_query_arg( array(), $wp->request );

if ( $current_slug == 'eventi' ) {
  $eventi = 'current';
  $calendario = '';
  $mappa = '';
} else if ( $current_slug == 'calendario' ) {
  $calendario = 'current';
  $eventi = '';
  $mappa = '';
} else {
  $mappa = 'current';
  $calendario = '';
  $eventi = '';
}

?>
<div class="events-menu">
  <div class="row--stuck txt-center">
    <div class="row__column mobile-4">
      <div class="events-menu__items <?php echo $eventi; ?>">
        <a class="events-menu__link" href="<?php bloginfo('url'); ?>/eventi/">
          <div class="visible--on-tab">
            <?php _e('Lista degli eventi', 'ae'); ?>
          </div>
          <div class="hidden--on-tab">
            <i class="mec-sl-list"></i>
          </div>
        </a>
      </div>
    </div>
    <div class="row__column mobile-4">
      <div class="events-menu__items <?php echo $calendario; ?>">
        <a class="events-menu__link" href="<?php bloginfo('url'); ?>/calendario/">
          <div class="visible--on-tab">
            <?php _e('Calendario', 'ae'); ?>
          </div>
          <div class="hidden--on-tab">
            <i class="mec-sl-calendar"></i>
          </div>
        </a>
      </div>
    </div>
    <div class="row__column mobile-4">
      <div class="events-menu__items <?php echo $mappa; ?>">
        <a class="events-menu__link" href="<?php bloginfo('url'); ?>/mappa-eventi/">
          <div class="visible--on-tab">
            <?php _e('Mappa', 'ae'); ?>
          </div>
          <div class="hidden--on-tab">
            <i class="mec-sl-location-pin"></i>
          </div>
        </a>
      </div>
    </div>
</div>
</div>
