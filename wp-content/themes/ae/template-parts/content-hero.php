<?php
/**
 * Template part for displaying page content in home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */
?>

<?php if ( get_field('enable_big_hero') == true) : ?>

  <header class="hero">
    <?php
    if ( get_field('hero_background_choosen') == 'immagine' && get_field('hero_background_image') ) {
      $background = 'style="background-image: url('.get_field('hero_background_image').')"';
    } else {
      $background = null;
    } ?>

    <div class="hero__content" <?php echo $background; ?>>
      <?php if ( get_field('hero_background_choosen') == 'video' ) : ?>
        <video playsinline class="vertical align js--onload js--start-video" poster="<?php echo get_field('hero_background_video_poster'); ?>" <?php if ( get_field('hero_background_video_loop') == true ) { echo 'loop'; } ?> muted>
          <source src="<?php echo get_field('hero_background_video_mp4'); ?>" type="video/mp4">
          <source src="<?php echo get_field('hero_background_video_ogv'); ?>" type="video/ogg">
          <source src="<?php echo get_field('hero_background_video_webm'); ?>" type="video/webm">
            <?php _e('Your browser does not support the video tag.', 'ae'); ?>
        </video>
      <?php elseif ( get_field('hero_background_choosen') == 'slideshow' ) : ?>
        <div class="hero__carousel owl-carousel js--hero-carousel">
          <?php
          $images = get_field('hero_slideshow');

          if( $images ): ?>
            <?php foreach( $images as $image ): ?>
              <div class="hero__carousel-single" style="background-image: url(<?php echo $image['url']; ?>"></div>
            <?php endforeach; ?>
          <?php endif; ?>
        </div>
      <?php endif; ?>

      <div class="hero__box txt-center">
        <div class="grid-container--small">
          <?php if(get_field('hero_title')): ?>
            <h1 class="hero__title"><?php echo get_field('hero_title') ?></h1>
          <?php endif; ?>

          <?php if(get_field('hero_text')): ?>
            <h2 class="hero__description antialiased"><?php echo get_field('hero_text') ?></h2>
          <?php endif; ?>
        </div>
      </div>

    </div><!-- .page__content -->

  </header><!-- .Hero -->

<?php endif; ?>
