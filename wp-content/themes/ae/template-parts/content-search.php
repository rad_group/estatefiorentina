<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>

  <div class="grid-container--small">
    <div class="row">
      <div class="row__column single-post__wrap">

        <?php get_template_part( 'template-parts/content', 'single-article' ); ?>

      </div>
    </div>
  </div>


</article><!-- #post-## -->
