<section class="newsletter">
  <div class="grid-container">
    <div class="newsletter__wrap txt-center cl">
      <h2 class="newsletter__title"><?php if(get_field('newsletter_title')){ echo get_field('newsletter_title'); } ?></h2>
      <h3 class="newsletter__subtitle"><?php if(get_field('newsletter_subtitle')){ echo get_field('newsletter_subtitle'); } ?></h3>
      <?php echo do_shortcode(get_field('newsletter_shortcode')); ?>
    </div>
  </div>
</section>
