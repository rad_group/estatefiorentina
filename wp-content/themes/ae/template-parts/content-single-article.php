<?php
if (get_post_type() == 'post') {
  $terms = wp_get_post_terms(get_the_ID(), 'category');
} elseif (get_post_type() == 'mec-events') {
  $terms = wp_get_post_terms(get_the_ID(), 'mec_category');
}

if ( has_post_thumbnail() ) {
  $post_thumb = get_the_post_thumbnail_url();
} else {
  $post_thumb = get_bloginfo('template_directory') . '/img/image-not-found.png';
}
?>

<article class="single-articolo <?php if (get_post_type() == 'post') { echo 'standard-post'; } else { echo 'event'; } ?>">
  <div class="row--stuck">
    <div class="row__column tab-5">
      <a class="single-articolo__link" href="<?php echo get_the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php if (get_post_type() == 'post') { ?>
          <div class="single-articolo__tag txt-left">
            <i class="mec-sl-pin"></i>
            <h4>News</h4>
          </div>
        <?php } else { ?>
          <div class="single-articolo__tag txt-left">
            <i class="mec-sl-calendar"></i>
            <h4>Evento</h4>
          </div>
        <?php } ?>

        <div class="single-articolo__img">
          <div class="single-articolo__thumb" style="background-image: url('<?php echo $post_thumb; ?>')">
          </div>
        </div>
      </a>
    </div>
    <div class="row__column tab-7">
      <div class="single-articolo__wrap">
        <?php foreach ($terms as $term) : ?>
          <a class="category-label <?php echo $term->slug; ?>" href="<?php echo get_term_link($term->term_id); ?>">
            <?php echo $term->name; ?>
          </a>
        <?php endforeach; ?>
        <a class="single-articolo__link" href="<?php echo get_the_permalink(); ?>" title="<?php the_title(); ?>">
          <div class="single-articolo__head">
            <h2 class="single-articolo__title">
              <?php echo get_the_title(); ?>
            </h2>
          </div>

          <?php if (get_post_type() == 'mec-events') : ?>
            <div class="single-articolo__info">
              <?php $getPostCustom = get_post_custom(); ?>
              <div class="single-articolo__date">
                <?php if ( $getPostCustom['mec_start_date'][0] ) : ?>
                  <i class="mec-sl-calendar"></i>
                  <span class="single-articolo__date--start"><?php echo $getPostCustom['mec_start_date'][0]; ?></span>
                <?php endif; ?>
                <?php if ( $getPostCustom['mec_start_date'][0] != $getPostCustom['mec_end_date'][0] ) : ?>
                  <span class="single-articolo__date--end"> • <?php echo $getPostCustom['mec_end_date'][0]; ?></span>
                <?php endif; ?>
              </div>
              <?php if (get_post_type() == 'mec-events') : ?>
                <?php $loc_terms = wp_get_post_terms(get_the_ID(), 'mec_location'); ?>
                <div class="single-articolo__place">
                  <?php foreach ($loc_terms as $loc_term) : ?>
                    <i class="mec-sl-location-pin"></i>
                    <span class="single-articolo__place-label">
                      <?php echo $loc_term->name; ?>
                    </span>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
        </a>
      </div>
    </div>
  </div>
</article>
