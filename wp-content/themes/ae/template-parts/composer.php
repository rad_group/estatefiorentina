<?php global $related_products; ?>

<?php if( have_rows('work_flex') ) : ?>

  <?php while ( have_rows('work_flex') ) : the_row(); ?>

    <?php if( get_row_layout() == 'primary_content' ) : ?>
      <div class="composer__block txt-center <?php echo get_margin() . get_bg_color(); ?>">

        <?php $selected_columns = composer_check();

        switch ( $selected_columns ) :

          // Only text selected
          case 0: ?>

            <div class="grid-container">
              <?php get_composer_txt(); ?>
            </div>

          <?php break;

          // Only image selected
          case 1: ?>

            <?php
            $position = get_sub_field_object('flex_primary_content_only_image_layout');
            $value = $position['value'];

            switch ($value) :
              case 'content_only_image_layout_left': // ONLY IMG LEFT ?>
                <div class="row--stuck">
                  <div class="row__column tab-6">
                    <?php echo get_composer_img('first'); ?>
                  </div>
                </div>
              <?php break;

              case 'content_only_image_layout_right': // ONLY IMG RIGHT ?>
              <div class="row--stuck">
                <div class="row__column tab-push-6 tab-6">
                  <?php get_composer_img('first'); ?>
                </div>
              </div>
              <?php break;

              case 'content_only_image_layout_middle': // ONLY IMG MIDDLE ?>
                <div class="grid-container--large">
                  <?php get_composer_img('first'); ?>
                </div>
              <?php break;

              case 'content_only_image_layout_full': // ONLY IMG FULL ?>
                <?php get_composer_img('first'); ?>
              <?php break;

              case 'content_only_image_layout_double': // DOUBLE ?>
                <div class="row--stuck">
                  <div class="row__column tab-6">
                    <?php get_composer_img('first'); ?>
                  </div>
                  <div class="row__column tab-6">
                    <?php get_composer_img('second'); ?>
                  </div>
                </div>
              <?php break; ?>
            <?php endswitch; ?>

          <?php break;

          // Text and image selected
          case 2:

            $position = get_sub_field_object('flex_primary_content_image_txt_layout');
            $value = $position['value'];

            switch ($value) {
              case 'content_image_txt_layout_left': // IMG+TXT LEFT ?>
                <div class="row--stuck columns-middle">
                  <div class="row__column tab-6">
                    <?php get_composer_img('first'); ?>
                  </div>
                  <div class="row__column tab-6">
                    <div class="composer__txt-only-wrap txt-left">
                      <?php get_composer_txt(); ?>
                    </div>
                  </div>
                </div>
              <?php break;

              case 'content_image_txt_layout_right': // IMG+TXT RIGHT ?>
                <div class="row--stuck columns-middle">
                  <div class="row__column tab-6">
                    <div class="composer__txt-only-wrap txt-left">
                      <?php get_composer_txt(); ?>
                    </div>
                  </div>
                  <div class="row__column tab-6">
                    <?php get_composer_img('first'); ?>
                  </div>
                </div>
              <?php break;
            } ?>

          <?php break;
        endswitch; ?>

      </div>
    <?php endif; ?>
    <?php if( get_row_layout() == 'flex_carousel' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?>">
        <?php $images = get_sub_field('flex_carousel_gallery'); ?>

        <?php if ( $images ) : ?>

          <div class="composer__carousel js--composer-carousel owl-carousel">
            <?php foreach( $images as $image ):

              if ( $image['alt'] ) {
                $image_alt = $image['alt'];
              } else {
                $image_alt = 'We RAD - Agenzia di Comunicazipne - Immagine del lavoro';
              } ?>
              <figure class="composer__carousel-figure">
                <img class="composer__carousel-single-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image_alt; ?>" >
              </figure>

            <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_video' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?>">
        <?php $overlay_txt = get_sub_field('flex_video_overlay_txt'); ?>
        <?php if( have_rows('flex_video_group') ): ?>
          <?php while( have_rows('flex_video_group') ): the_row(); ?>

            <?php if ( get_sub_field('flex_video_layout') == 'middle' ) {
              $video_grid = 'grid-container--large';
            } else {
              $video_grid = null;
            }?>

            <div class="composer__video <?php echo $video_grid; ?>">
              <?php
              $video_autoplay = get_sub_field('flex_video_autoplay');
              $video_mute = get_sub_field('flex_video_mute');
              $video_poster = get_sub_field('flex_video_poster');
              $video_mp4 = get_sub_field('flex_video_mp4');
              $video_webm = get_sub_field('flex_video_webm');

              if ( $video_autoplay ) {
                $flex_autoplay = 'autoplay loop';
              } else {
                $flex_autoplay = null;
              }

              if ( $video_mute ) {
                $flex_mute = 'muted';
              } else {
                $flex_mute = null;
              }
              ?>

              <section class="home-video">
                <div class="video-ui js--video-ui">
                  <?php if ( $overlay_txt ) : ?>
                    <div class="video__overlay">
                      <h2 class="composer__txt-title--white vertical-align grid-container txt-center">
                        <?php echo $overlay_txt; ?>
                      </h2>
                    </div>
                  <?php endif; ?>
                  <video class="video-ui__video <?php echo $flex_autoplay; ?>" preload="metadata" poster="<?php echo $video_poster; ?>" <?php echo $flex_autoplay; ?> <?php echo $flex_mute; ?>>
                    <source src="<?php echo $video_mp4; ?>" type="video/mp4">
                    <source src="<?php echo $video_webm; ?>" type="video/webm">
                  </video>
                  <?php if ( $video_autoplay != true) : ?>
                  <div class="video-ui__playpause">
                    <img src="<?php bloginfo('template_url'); ?>/img/rad-play.svg" alt="ui-video-play">
                  </div>
                <?php endif; ?>
                </div>
              </section>

            </div>

          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_devices' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?>">
        <?php if( have_rows('flex_devices_group') ): ?>
          <?php while( have_rows('flex_devices_group') ): the_row(); ?>

            <?php

            $device_model = get_sub_field('flex_devices_model');

            switch ($device_model) {
              case 'desktop': ?>
                <div class="grid-container--large">
                  <div class="composer__devices-wrap--desk">
                    <img src="<?php bloginfo('template_url'); ?>/img/device_template/rad_thunderbolt_display.png" alt="Siti Web Rad - Anteprima Desktop">
                    <div class="composer__devices-screen--desk">
                      <?php get_devices_media_type(); ?>
                    </div>
                  </div>
                </div>
              <?php break;

              case 'tablet': ?>
                <div class="grid-container">
                  <div class="composer__devices-wrap--tab">
                    <img src="<?php bloginfo('template_url'); ?>/img/device_template/rad_ipad_pro.png" alt="Siti Web Rad - Anteprima Tablet">
                    <div class="composer__devices-screen--tab">
                      <?php get_devices_media_type(); ?>
                    </div>
                  </div>
                </div>
              <?php break;

              case 'mobile': ?>
                <div class="grid-container--small">
                  <div class="composer__devices-wrap--mobile">
                    <img src="<?php bloginfo('template_url'); ?>/img/device_template/rad_iphone8.png" alt="Siti Web Rad - Anteprima Mobile">
                    <div class="composer__devices-screen--mobile">
                      <?php get_devices_media_type(); ?>
                    </div>
                  </div>
                </div>
              <?php break;
            } ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_related' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?>">
        <div class="grid-container--large">
          <h2 class="composer__related-title txt-center"><?php echo get_sub_field('flex_related_title'); ?></h2>

          <?php
          $posts = get_sub_field('flex_related_article');
          if ($posts) :
            $args = array(
              'post_type'      => 'product',
              'posts_per_page' => 3,
              'post__in' => $posts,
            );

            $loop = new WP_Query( $args ); ?>

            <div class="row">
              <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

              <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>
            </div>

          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>

  <?php endwhile; ?>

<?php endif; ?>

<?php
/*
 * Check if selected columns (txt, img or both)
 * $selected == 0 --> Only Text
 * $selected == 1 --> Only Image
 * $selected == 2 --> Image and Text
 */
function composer_check() {
  $check_columns = get_sub_field_object('primary_content_columns');
  $value = $check_columns['value'];

  if ( count($value) > 1 ) {
    $selected = 2;
  } else {
    for ($c = 0; $c <= count($value); $c++) {
      if ( isset($value[$c]) && $value[$c] == 'single_txt' ) {
        $selected = 0;
      } else if ( isset($value[$c]) && $value[$c] == 'single_img' ) {
        $selected = 1;
      }
    }
  }

  return $selected;
}

function get_composer_txt() {

  if( have_rows('flex_txt_group') ):
    while( have_rows('flex_txt_group') ): the_row();

      $prefix_title = get_sub_field('flex_primary_content_prefix');
      $primary_title = get_sub_field('flex_primary_content_title');
      $underscore = get_sub_field('flex_primary_content_title_underscore');
      $standard_txt = get_sub_field('flex_primary_content_txt');
      $text_columns = get_sub_field('flex_primary_content_txt_columns');
      $label = get_sub_field('flex_primary_button_label');
      $check_link = get_sub_field('flex_primary_link');
      $inner_link = get_sub_field('flex_primary_inner_link');
      $outer_link = get_sub_field('flex_primary_outer_link');

      $columns = '';
      if ( $text_columns == 'double' ):
        $columns = ' double';
      endif;
      ?>


      <div class="composer__txt-only <?php echo $columns; ?>">
        <?php if ( $prefix_title ) : ?>
          <h2 class="composer__txt-prefix"><?php echo $prefix_title; ?></h2>
        <?php endif; ?>

        <?php if ( $primary_title ) : ?>
          <h2 class="composer__txt-title"><?php echo $primary_title; ?></h2>
        <?php endif; ?>

        <?php if ( $standard_txt ) : ?>
          <div class="composer__txt-only--content formatted-content<?php echo $columns;  ?>">
            <?php echo $standard_txt; ?>
          </div>
        <?php endif; ?>

        <?php if ( $label && $label != '' ) :
          if ($check_link == 'inner') :
            $link = $inner_link;
          else:
            $link = $outer_link;
          endif; ?>
          <div class="composer__txt-link formatted-content">
            <a class="button" href="<?php echo $link; ?>" title="<?php echo $label; ?>"><?php echo $label; ?></a>
          </div>
        <?php endif; ?>
      </div>

    <?php

    endwhile;
  endif;

}

function get_composer_img($im) {

  if( have_rows('flex_img_group') ):
    while( have_rows('flex_img_group') ): the_row();

      $first_img = get_sub_field('flex_primary_content_img');
      $second_img = get_sub_field('flex_primary_content_img_second');
      ?>

      <div class="composer__img-wrap">
        <?php
        if ( $im == 'first' ) {
          if ( $first_img ) {
            echo '<img class="composer__img" src="' . $first_img . '">';
          }
        } elseif ( $im == 'second' ) {
          if ( $second_img ) {
            echo '<img class="composer__img" src="' . $second_img . '">';
          }
        }
        ?>
      </div>

    <?php
    endwhile;
  endif;

}

function get_devices_media_type() {
  $media_type = get_sub_field('flex_devices_media_type');
  $static_img = get_sub_field('flex_devices_img_static');
  $video_mp4 = get_sub_field('flex_devices_video_mp4');
  $video_webm = get_sub_field('flex_devices_video_webm');
  $video_poster = get_sub_field('flex_devices_video_poster');

  if ( $media_type == 'img' ) {
    if ( $static_img['alt'] ) {
      $static_alt = $static_img['alt'];
    } else {
      $static_alt = 'We RAD - Agenzia di Comunicazipne - Immagine del lavoro';
    } ?>
    <img src="<?php echo $static_img['url']; ?>" alt="<?php echo $static_alt; ?>">
  <?php
  } else { ?>
    <video preload="metadata" poster="<?php echo $video_poster; ?>" autoplay muted loop>
      <source src="<?php echo $video_mp4; ?>" type="video/mp4">
      <source src="<?php echo $video_webm; ?>" type="video/webm">
    </video>
  <?php
  }
}

function get_margin() {

  $margin_top = get_sub_field('flex_margin_top');
  $margin_bottom = get_sub_field('flex_margin_bottom');

  if ( $margin_top ) {
    $margin_top_class = 'composer__margin-top--' . $margin_top;
  } else {
    $margin_top_class = null;
  }

  if ( $margin_bottom ) {
    $margin_bottom_class = 'composer__margin-bottom--' . $margin_bottom;
  } else {
    $margin_bottom_class = null;
  }

  return $margin_top_class . ' ' . $margin_bottom_class;

}

function get_bg_color() {

  $background_color = get_sub_field('flex_background_color');

  $bg_color = '';
  if ( $background_color == 'gray') :
    $bg_color = ' gray';
  endif;

  return $bg_color;

}
