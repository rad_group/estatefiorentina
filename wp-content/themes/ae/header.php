<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framaework
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!-- favicon -->
    <?php $favicon = get_field('favicon_image', 'option'); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php if ( $favicon['sizes']['medium_large'] ) { echo $favicon['sizes']['medium_large']; } else { echo get_bloginfo('template_url') . '/favicons/apple-touch-icon.png'; } ?>">
    <link rel="icon" type="image/png" href="<?php if ( $favicon['sizes']['favicon-32'] ) { echo $favicon['sizes']['favicon-32']; } else { echo get_bloginfo('template_url') . '/favicons/favicon-32x32.png'; } ?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php if ( $favicon['sizes']['favicon-16'] ) { echo $favicon['sizes']['favicon-16']; } else { echo get_bloginfo('template_url'). '/favicons/favicon-16x16.png'; }; ?>" sizes="16x16">
    <meta name="theme-color" content="#ffffff">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-90544096-10"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-90544096-10');
    </script>

    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    <div id="page" class="site">

      <header class="mobile-header hidden--on-tab">
        <div class="row--stuck columns-middle">
          <div class="row__column mobile-8">
            <div class="hamburger js--toggle-menu">
              <span class="hamburger__elements">
                <span></span>
              </span>
            </div>
            <a class="mobile-logo" href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">
              <img src="<?php bloginfo('template_directory'); ?>/img/mobile-logo.svg" title="Estate Fiorentina 2019">
            </a>
          </div>
          <div class="row__column mobile-4 txt-right">
            <div class="mobile-header__search js--popup-action" data-popup="search-popup">
              <i class="iconae ae--search light x2"></i>
            </div>
          </div>
        </div>
      </header>

      <div class="main-sidebar">
        <div class="main-sidebar__logo">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img src="<?php bloginfo('template_directory'); ?>/img/main-logo.svg" alt="Estate Fiorentina 2019">
          </a>
        </div>
        <?php bem_menu('primary', 'primary-menu', 'js--mobile-menu'); ?>

        <div class="social-menu">
          <ul class="social__list">
            <li class="social__list-item">
              <a href="https://www.facebook.com/estatefiorentina/" class="social__link" target="_blank" title="Facebook">
                <i class="iconae filled ae--facebook-square"></i>
              </a>
            </li>
            <li class="social__list-item">
              <a href="https://www.instagram.com/estate_fiorentina/" class="social__link" target="_blank" title="Instagram">
                <i class="iconae filled ae--instagram"></i>
              </a>
            </li>
            <li class="social__list-item">
              <a href="https://twitter.com/estatefi" class="social__link" target="_blank" title="Twitter">
                <i class="iconae filled ae--twitter-square"></i>
              </a>
            </li>
          </ul>
        </div>

        <div class="main-sidebar__bottom">
          <!-- <div class="main-sidebar__firenze txt-center">
            <img src="<?php //bloginfo('template_directory'); ?>/img/citta_di_firenze.svg" alt="Città di Firenze">
          </div> -->
          <a class="main-sidebar__bottom-locandine" href="https://estatefiorentina.it/wp-content/uploads/2019/09/Locandine-EF19.zip" title="scarica le locandine">Scarica le locandine</a>
          <div class="main-sidebar__search js--popup-action" data-popup="search-popup">
            <i class="iconae ae--search light"></i> Cerca nel sito
          </div>
        </div>

      </div>

      <div id="content" class="site-content">
