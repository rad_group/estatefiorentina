<?php
/** no direct access **/
defined('MECEXEC') or die();

/**
 * The Template for displaying all single events
 *
 * @author Webnus <info@webnus.biz>
 * @package MEC/Templates
 * @version 1.0.0
 */
get_header('mec'); ?>

  <?php do_action('mec_before_main_content'); ?>

    <section id="<?php echo apply_filters('mec_single_page_html_id', 'main-content'); ?>">
      <?php while(have_posts()): the_post(); ?>

        <?php $MEC = MEC::instance(); echo $MEC->single(); ?>

      <?php endwhile; // end of the loop. ?>
      <?php //comments_template(); ?>
    </section>

    <div class="scroller-status">
      <div class="infinite-scroll-request loader-ellips"></div>
    </div>
    <p class="pagination">
      <?php next_posts_link('', $loop->max_num_pages); ?>
    </p>

  <?php do_action('mec_after_main_content'); ?>

<?php get_footer('mec');
