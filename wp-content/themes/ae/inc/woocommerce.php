<?php
// Declare Woocommerce theme support
add_theme_support('woocommerce');

// Remove Standard Woocommerce CSS Style
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Add a custom wrap around Woocommerce elements
add_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper');
// overwrite existing output content wrapper function
function woocommerce_output_content_wrapper() {
  echo '<div class="grid-container--large">';
}
add_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end');
function woocommerce_output_content_wrapper_end() {
  echo '</div>';
}

// Remove sidebar from Woocommerce single product page
add_action( 'wp', 'ae_remove_sidebar_product_pages' );

function ae_remove_sidebar_product_pages() {
  if ( is_product() ) {
    remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
  }
}

// Remove cross-sells products from cart
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

//* Do NOT include the opening php tag shown above. Copy the code shown below into functions.php
/**
 * Manage WooCommerce styles and scripts.
 */
function grd_woocommerce_script_cleaner() {

	// Remove the generator tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
	// Unless we're in the store, remove all the cruft!
		wp_dequeue_style( 'woocommerce_frontend_styles' );
		wp_dequeue_style( 'woocommerce-general');
		wp_dequeue_style( 'woocommerce-layout' );
		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce_fancybox_styles' );
	  wp_dequeue_style( 'woocommerce_chosen_styles' );
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		// wp_dequeue_script( 'selectWoo' );
		// wp_deregister_script( 'selectWoo' );
		// wp_dequeue_script( 'wc-add-payment-method' );
		// wp_dequeue_script( 'wc-lost-password' );
		// wp_dequeue_script( 'wc_price_slider' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-add-to-cart' );
		// wp_dequeue_script( 'wc-cart-fragments' );
		// wp_dequeue_script( 'wc-credit-card-form' );
		// wp_dequeue_script( 'wc-checkout' );
		// wp_dequeue_script( 'wc-add-to-cart-variation' );
		wp_dequeue_script( 'wc-single-product' );
		// wp_dequeue_script( 'wc-cart' );
		// wp_dequeue_script( 'wc-chosen' );
		// wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'prettyPhoto' );
		wp_dequeue_script( 'prettyPhoto-init' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jquery-placeholder' );
		// wp_dequeue_script( 'jquery-payment' );
		wp_dequeue_script( 'fancybox' );
		wp_dequeue_script( 'jqueryui' );
}
add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );
