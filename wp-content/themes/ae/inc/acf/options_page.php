<?php
if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'Favicon',
		'menu_title'	=> 'Favicon Settings',
		'menu_slug' 	=> 'favicon-general-settings',
		'redirect'		=> false
  ));

}
