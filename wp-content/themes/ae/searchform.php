<form role="search" method="get" class="search-form search-bar" action="<?php bloginfo('url'); ?>">
  <div class="form__field">
    <label class="input-group">
      <input type="search" class="search-field" placeholder="Ricerca nel sito..." value="" name="s">
      <div class="input-group__button">
        <input class="search-form__icn" type="image" alt="Search" src="<?php bloginfo( 'template_url' ); ?>/img/search.svg" />
      </div>
    </label>
  </div>
</form>
