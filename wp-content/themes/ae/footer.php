<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framaework
 */

?>
    <div class="home-article__bottom">
      <div class="home-article__bg">
        <img src="<?php bloginfo('template_url'); ?>/img/home-blog-top.png" alt="Background Estate Fiorentina">
      </div>
      <img class="home-article__bottom-bg-illustration" src="<?php bloginfo('template_url'); ?>/img/estatefiorentina-illustrazione-footer.svg" alt="Illustrazione Estate Fiorentina Footer">
      <?php if(is_front_page()) { ?>
        <div class="home-article__btn vertical-align">
          <a href="<?php bloginfo('url'); ?>/eventi/" class="button">Tutti gli eventi</a>
        </div>
      <?php } ?>

    </div>
  </div><!-- #content -->

  <footer class="site-footer sponsor-footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
    <div class="sponsor-footer__wrap">
      <div class="row--stuck">
        <div class="row__column tab-6">
          <div class="sponsor-footer__logo">
            <img src="<?php bloginfo('template_url'); ?>/img/logo-footer.svg" alt="Estate Fiorentina 2019">
            <!-- <p> Sei mesi, centocinquanta spazi in città coinvolti, oltre cento associazioni culturali, centinaia di iniziative culturali in programma. Questi sono i numeri della nostra lunga estate fiorentina. Un formato esteso nel tempo e diffuso, che ha il merito di cucire associazioni e luoghi anche meno centrali della nostra città. </p> -->
          </div>
        </div>
        <div class="row__column tab-3 sponsor-footer__mainsponsor txt-left">
          <p> Main Sponsor </p>
          <img src="<?php bloginfo('template_url'); ?>/img/toscanaenergia-logo.svg" alt="Toscana Energia Logo">
        </div>
        <div class="row__column tab-3 sponsor-footer__sponsor txt-left">
          <p> Sponsor </p>
          <div class="sponsor-footer__sponsor-logo">
            <img src="<?php bloginfo('template_url'); ?>/img/sponsor-footer.svg" alt="Logos">
          </div>
        </div>
      </div>

    </div>

    <div class="sponsor-footer__info">
      <div class="row--stuck columns-middle">
        <div class="row__column tab-8">
          © <a href="<?php bloginfo('wpurl') ?>" title="<?php bloginfo('name') ?>"><?php bloginfo('name') ?></a> All rights reserved. Sito realizzato da <a href="https://we-rad.com" target="_blank" title="Rad Group"><strong>WE RAD</strong></a>.
        </div>
        <div class="row__column tab-4">
          <div class="sponsor-footer__social">
            <ul class="inline-li">
              <li class="social__list-link">
                <a href="https://www.facebook.com/estatefiorentina/" class="social__link" target="_blank" title="Facebook">
                  <i class="iconae ae--facebook-square filled"></i>
                </a>
              </li>
              <li class="social__list-link">
                <a href="https://www.instagram.com/estate_fiorentina/" class="social__link" target="_blank" title="Instagram">
                  <i class="iconae ae--instagram filled"></i>
                </a>
              </li>
              <li class="social__list-link">
                <a href="https://twitter.com/estatefi" class="social__link" target="_blank" title="Twitter">
                  <i class="iconae ae--twitter-square filled"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- #colophon -->

  <div class="js--popup popup--custom-full" id="search-popup">

    <div class="popup__wrap">
      <button class="js--close-popup popup__button"><span class="popup__button-label">CHIUDI</span><i class="iconae light x2 ae--cross"></i></button>
      <div class="vertical-align grid-container--large grid-container-custom-search">
        <div class="search-input">
          <img src="<?php bloginfo('template_url'); ?>/img/search-popup.svg" alt="Search BG">
          <div class="search-input__wrap vertical-align">
            <?php get_search_form(); ?>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- <a href="#" class="js--back-to-top-btn back-to-top-btn">
    <i class="iconae regular ae--arrow-up"></i>
  </a> -->

</div><!-- #page -->

<div class="site-overlay"></div>

<?php wp_footer(); ?>

</body>
</html>
