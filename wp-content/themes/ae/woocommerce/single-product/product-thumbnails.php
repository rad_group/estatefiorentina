<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.2
 */

defined( 'ABSPATH' ) || exit;

global $post, $product;
//global $slider_layout;

$attachment_ids = $product->get_gallery_image_ids();
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
array_unshift($attachment_ids,$post_thumbnail_id);

$image_size = 'full';

// if($slider_layout):
// 	$image_size = 'slider-thumb';
// endif;

//print_r($attachment_ids);
if ( $attachment_ids && has_post_thumbnail() ) {
	$id = 0;
	foreach ( $attachment_ids as $attachment_id ) {

		$src_image = wp_get_attachment_image_src( $attachment_id, $image_size);
		echo '<img src="'.$src_image[0].'" class="bgdet" title="'.get_post_field( 'post_title', $attachment_id ).'" data-caption="'.get_post_field( 'post_excerpt', $attachment_id ).'" />';
		// $html = wp_get_attachment_image( $attachment_id, $image_size, false, $attributes );
		//
		// echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
		$id++;
	}
}
