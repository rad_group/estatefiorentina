<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--large">
  <div class="row">
    <div class="row__column tab-8">
      <main id="main" class="site-main" role="main">

        <?php
        if ( have_posts() ) : ?>

          <header class="page-header">
            <?php the_archive_title( '<h1 class="page-header__page-title">', '</h1>' ); ?>
            <?php the_archive_description( '<div class="page-header__archive-description">', '</div>' ); ?>
          </header><!-- .page-header -->

        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post();

          /*
           * Include the Post-Format-specific template for the content.
           * If you want to override this in a child theme, then include a file
           * called content-___.php (where ___ is the Post Format name) and that will be used instead.
           */
          get_template_part( 'template-parts/content', get_post_format() );

        endwhile;

          the_posts_navigation();

        else :

          get_template_part( 'template-parts/content', 'none' );

        endif; ?>

      </main><!-- #main -->
    </div>

    <div class="row__column tab-4">
      <?php get_sidebar(); ?>
    </div>

  </div> <!-- .row -->
</div>
<?php get_footer(); ?>
