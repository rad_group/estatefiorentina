$(document).ready(function(){
  var popup_trigger = $('.js--popup-action');

  if( popup_trigger.data('popup') == 'search-popup' ) {

    popup_trigger.click(function(){

      if ( $(this).hasClass('clicked') ) {
        $(this).removeClass('clicked');
        $('.js--popup').removeClass('active');
        $('body').removeClass('popup-opened');
        $('.mobile-header__search .iconae').addClass('ae--search');
        $('.mobile-header__search .iconae').removeClass('ae--cross');
      } else {
        $(this).addClass('clicked');
        $('.mobile-header__search .iconae').removeClass('ae--search');
        $('.mobile-header__search .iconae').addClass('ae--cross');
      }

      // Focus on input
      setTimeout(function(){
        $('.search-field').focus();
      },100);
    });

  }

});
