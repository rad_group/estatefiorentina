function qnty() {
  jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
  jQuery('.quantity').each(function() {
    var qnty = jQuery(this),
      input = qnty.find('input[type="number"]'),
      btnUp = qnty.find('.quantity-up'),
      btnDown = qnty.find('.quantity-down'),
      min = input.attr('min'),
      max = input.attr('max');
    if (max == "") {
      max = 100;
    }

    btnUp.click(function() {
      var oldValue = parseFloat(input.val());
      max = input.attr('max');
      if (max == "") {
        max = 100;
      }
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      input.val(newVal);
      qnty.find("input").val(newVal);
      qnty.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      qnty.find("input").val(newVal);
      qnty.find("input").trigger("change");
    });
  });
}

$(document).ready(function() {
  qnty();
  $('select').blur( function() {
    if( '' != $('input.variation_id').val() ) {
      $('p.price').html($('div.woocommerce-variation-price > span.price').html()).append('<p class="availability">'+$('div.woocommerce-variation-availability').html()+'</p>');
      console.log($('input.variation_id').val());
    } else {
      $('p.price').html($('div.hidden-variable-price').html());
      if( $('p.availability') )
        $('p.availability').remove();
      console.log('NULL');
    }
  });
});

$( document.body ).on( 'updated_cart_totals', function(){
  qnty();
});
