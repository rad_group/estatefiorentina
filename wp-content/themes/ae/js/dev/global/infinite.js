// Document Ready
$( document ).ready(function() {

  $('.mec-wrap .row').infiniteScroll({
    // options
    path: '.pagination a',
    append: '.row__column.desk-6',
    history: false,
    status: '.scroller-status',
    hideNav: '.pagination',
  });

  $('.primary-wrap .row').infiniteScroll({
    // options
    path: '.pagination a',
    append: '.row__column.tab-12.desk-6',
    history: false,
    status: '.scroller-status',
    hideNav: '.pagination',
  });
  //
  $('.search-content .row').infiniteScroll({
    // options
    path: '.pagination a',
    append: '.row__column.tab-6.desk-6',
    history: false,
    status: '.scroller-status',
    hideNav: '.pagination',
  });


}); //END Document Ready
