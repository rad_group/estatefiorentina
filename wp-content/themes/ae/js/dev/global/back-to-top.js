// Document Ready
$( document ).ready(function() {
  var $back_to_top = $('.js--back-to-top-btn');
  // Back to Top Button
  if ($back_to_top.length) {
    var scrollTrigger = 100, // px
    back_to_top = function() {
      var scroll_top = $(window).scrollTop();
      if (scroll_top > scrollTrigger) {
        $back_to_top.addClass('show');
      } else {
        $back_to_top.removeClass('show');
      }
    };
    back_to_top();
    $(window).on('scroll', function() {
      back_to_top();
    });
    $back_to_top.on('click', function(e) {
      e.preventDefault();
      $('html,body').animate({
        scrollTop: 0
      }, 700);
    });
  }

});
