// Document Ready
$( document ).ready(function() {
  if ( $('.woocommerce-tabs').length ) {
    $('.woocommerce-tabs').find('.description_tab').addClass('current-tab');
    $('.woocommerce-Tabs-panel#tab-description').addClass('shown-tab');
    tab();
  }
}); //END Document Ready

function tab() {

  $('.woocommerce-tabs').each(function() {

    var tab_trigger = $(this).find('.wc-tabs li');

    tab_trigger.click(function(e) {
      e.preventDefault();
      var tab_trigger_attr = $(this).attr('aria-controls');

      tab_trigger.removeClass('current-tab');
      $(this).addClass('current-tab');

      $('.woocommerce-Tabs-panel').removeClass('shown-tab');
      $('#' + tab_trigger_attr).addClass('shown-tab');

    });

  });
}
