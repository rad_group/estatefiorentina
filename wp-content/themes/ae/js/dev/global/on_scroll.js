$(document).ready(function(){
  onScroll('.filter-wrap');
});

$(window).on('scroll', function() {
  onScroll('.filter-wrap');
});

function onScroll(target) {
  var $target = $(target);
  var scrollTrigger = 50;
  var scroll_top = $(window).scrollTop();

  if (scroll_top > scrollTrigger) {
    $target.addClass('on-scroll');
  } else {
    $target.removeClass('on-scroll');
  }
}
