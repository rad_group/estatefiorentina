<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">

  <div class="single-post__header-offset--news"></div>
  <header class="single-post__header--news">
    <h1 class="single-post__header-title"><i class="mec-sl-pin"></i> <?php _e('Le news di Estate Fiorentina', 'ae'); ?></h1>
  </header>

  <div class="primary-wrap">
    <div class="row">
      <?php
      $set_date = get_field('post_date', 6688);
      ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <?php
        if($set_date < $post->post_date) {
          $hide = '';
        } else {
          $hide = 'hide';
        }
        ?>

        <div class="row__column tab-12 desk-6 <?php echo $hide; ?> ">
          <?php get_template_part( 'template-parts/content', 'single-article' ); ?>
        </div>

      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>
    <div class="scroller-status">
      <div class="infinite-scroll-request loader-ellips"></div>
    </div>
    <p class="pagination">
      <?php next_posts_link(); ?>
    </p>
  </div>

</main><!-- #main -->

<?php get_footer(); ?>
