<?php
/**
 * framaework functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package framaework
 */

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Custom functions that act independently of the theme templates.
require get_template_directory() . '/inc/extras.php';

// Include Elements
require get_template_directory() . '/inc/file_calling.php';

// Merge CSS and JS files into one
if ( !is_admin() && $pagenow != 'wp-login.php' ) {
  //require get_template_directory() . '/inc/minification.php';
  require get_template_directory() . '/inc/merging.php';
}

// Inculde Widgets
require get_template_directory() . '/inc/widgets.php';

// Change the menu layout with Bem Semantic
require get_template_directory() . '/inc/bem_menu.php';

require get_template_directory() . '/inc/acf/options_page.php';

// Woocommerce calling - If Woocommerce isn't installed, this file will be ignored
if ( class_exists( 'WooCommerce' ) ) {
  require get_template_directory() . '/inc/woocommerce.php';
}

require get_template_directory() . '/inc/import_plugins/plugins-calling.php';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'ae_setup' ) ) :

  function ae_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on framaework, use a find and replace
     * to change 'ae' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'ae', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    /*
     * Enable Custom Images Crop
     */
    add_image_size( 'favicon-16', 16, 16, false );
    add_image_size( 'favicon-32', 32, 32, false );
    add_image_size( 'favicon-apple-touch', 180, 180, false );

    add_image_size( 'article_thumb_long', 490, 200, true );
    add_image_size( 'article_thumb_short', 490, 90, true );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
      'primary' => __( 'Primary Menu', 'ae' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Set Excerpt length
    function custom_excerpt_length( $length ) {
      return 36;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

    // Set Read More at the excerpt end
    function new_excerpt_more( $more ) {
      return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'ae') . '</a>';
    }
    add_filter( 'excerpt_more', 'new_excerpt_more' );

    // Remove Emoji
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );

    // Remove Embeded JS (Wordpress funzionality to embed other people's WordPress posts into your own WordPress posts)
    function my_deregister_scripts() {
      wp_deregister_script( 'wp-embed' );
    }
    add_action( 'wp_footer', 'my_deregister_scripts' );

    function get_post_custom_fields() {
      $getPostCustom = get_post_custom();
      foreach($getPostCustom as $name=>$value) {

          echo "<strong>".$name."</strong>"."  =>  ";

          foreach($value as $nameAr=>$valueAr) {
                  echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                  echo $nameAr."  =>  ";
                  echo var_dump($valueAr);
          }

          echo "<br /><br />";

      }
    }

  }
  
  function sb_add_cpts_to_api( $args, $post_type ) {
    if ( 'mec-events' === $post_type ) {
        $args['show_in_rest'] = true;
        //$args['public'] = true;
    }
    return $args;
  }
  add_filter( 'register_post_type_args', 'sb_add_cpts_to_api', 10, 2 );
  
  register_rest_field( 'mec-events', 'metadata', array(
      'get_callback' => function ( $data ) {
          return get_post_meta( $data['id'], '', '' );
      }, ));
  
  endif;
add_action( 'after_setup_theme', 'ae_setup' );
