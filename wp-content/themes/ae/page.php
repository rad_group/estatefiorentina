<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <?php while ( have_posts() ) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

      <?php if ( get_field('enable_big_hero') == true) : ?>
        <?php get_template_part( 'template-parts/content', 'hero' ); ?>
      <?php else : ?>
        <header class="page__header">
          <div class="grid-container--large">
            <?php the_title( '<h1 class="page__title">', '</h1>' ); ?>
          </div>
        </header><!-- .page__header -->
      <?php endif; ?>

      <div class="grid-container--large">
        <div class="page__content">
          <?php the_content( 'template-parts/composer' ); ?>
        </div><!-- .page__content -->
      </div>

    </article><!-- #post-## -->

  <?php endwhile; ?>
</main><!-- #main -->
<?php get_footer(); ?>
