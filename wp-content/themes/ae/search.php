<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package framaework
 */

get_header(); ?>

  <main id="main" class="site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

  <?php if ( have_posts() ) : ?>

    <div class="page-header__header-offset"></div>
    <header class="page-header">
      <h1 class="page-header__page-title">
        <?php printf( esc_html__( 'Search Results for: %s', 'ae' ), '<span>' . get_search_query() . '</span>' ); ?>
      </h1>
    </header><!-- .page-header -->

    <div class="search-content">
      <div class="row">
        <?php
        while ( have_posts() ) : the_post(); ?>

          <div class="row__column tab-6 desk-6">
            <?php get_template_part( 'template-parts/content', 'single-article' ); ?>
          </div>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>
      </div>

      <div class="scroller-status">
        <div class="infinite-scroll-request loader-ellips"></div>
      </div>
      <p class="pagination">
        <?php next_posts_link(); ?>
      </p>

    </div>

  <?php
  else :
    get_template_part( 'template-parts/content', 'none' );
  endif; ?>

  </main><!-- #main -->


<?php get_footer(); ?>
