<?php
/** no direct access **/
defined('MECEXEC') or die();

/**
 * The Template for displaying mec-category taxonomy events
 *
 * @author Webnus <info@webnus.biz>
 * @package MEC/Templates
 * @version 1.0.0
 */
get_header('mec'); ?>
<?php
$terms = wp_get_post_terms(get_the_ID(), 'category');
?>
	<?php do_action('mec_before_main_content'); ?>

    <section id="<?php echo apply_filters('mec_category_page_html_id', 'main-content'); ?>" class="<?php echo apply_filters('mec_category_page_html_class', 'container'); ?>">
			<?php if(have_posts()): ?>

				<?php do_action('mec_before_events_loop'); ?>

				<div class="single-post__header-offset"></div>
				<header class="single-post__header">
					<div class="grid-container--large">
						<?php foreach ($terms as $term) : ?>
							<div class="single-post__header-category"><?php echo $term->name; ?></div>
						<?php endforeach; ?>
						<h1 class="single-post__header-title"><?php echo single_term_title(''); ?></h1>
					</div>
				</header>

				<?php $MEC = MEC::instance(); echo $MEC->category(); ?>

				<?php do_action('mec_after_events_loop'); ?>

			<?php endif; ?>
    </section>

		<div class="scroller-status">
      <div class="infinite-scroll-request loader-ellips"></div>
    </div>
    <p class="pagination">
      <?php next_posts_link('', $loop->max_num_pages); ?>
    </p>

  <?php do_action('mec_after_main_content'); ?>

<?php get_footer('mec');
