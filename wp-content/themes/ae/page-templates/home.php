<?php
/**
 *
 * Template Name: Home Page
 *
 */

get_header();
?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <?php
  $home_highlight_1 = get_field('home_highlight_post_1');
  $home_highlight_2 = get_field('home_highlight_post_2');
  $home_highlight_3 = get_field('home_highlight_post_3');
  ?>

  <header class="home-highlights">
    <?php for ($i=1; $i < 4; $i++) : ?>
      <?php $home_highlight_ = 'home_highlight_' . $i; ?>
      <?php if (get_post_type(${$home_highlight_}) == 'post') {
        $terms = wp_get_post_terms(${$home_highlight_}, 'category');
      } elseif (get_post_type(${$home_highlight_}) == 'mec-events') {
        $terms = wp_get_post_terms(${$home_highlight_}, 'mec_category');
      } ?>
        <article class="home-highlights__article--<?php echo $i; ?>" style="background-image: url(<?php echo get_the_post_thumbnail_url(${$home_highlight_}); ?>)">
          <a class="home-highlights__article-link" href="<?php echo get_the_permalink(${$home_highlight_}); ?>" title="<?php echo get_the_title(${$home_highlight_}); ?>">
            <div class="gradient-bg"></div>
            <div class="home-highlights__article-title-wrap">
              <?php foreach ($terms as $term) : ?>
                <div class="category-label <?php echo $term->slug; ?>"><?php echo $term->name; ?></div>
              <?php endforeach; ?>
              <h2 class="home-highlights__article-title"><?php echo get_the_title(${$home_highlight_}); ?></h2>
            </div>
          </a>
        </article>
    <?php endfor; ?>
  </header>

  <?php do_action('mec_before_main_content'); ?>

<!-- SEZIONE HOME PAGE ULTIMI ARTICOLI/EVENTI -->
  <?php
  $acf_post_type = get_field('home_post_type_query');
  $current_date = date('Y-m-d');
  $set_date = get_field('post_date', 6688);
  ?>

  <?php
  switch ($acf_post_type) {
    case 'post':
      $post_types = 'post';
      $date = 'date';
      $meta_query = null;
      $link = $term->ID;
    break;
    case 'event':
      $post_types = 'mec-events';
      $date = 'mec_start_date';
      $link = $term->term_id;
      $meta_query = array(
        array(
          'relation'    => 'AND',
          array(
            'key'          => 'mec_start_date',
            'value'        => $current_date,
            'compare'      => '<=',
          ),
          array(
            'key'          => 'mec_end_date',
            'value'        => $current_date,
            'compare'      => '>=',
          )
        ),
      );
    break;
    case 'both':
      $post_types = array('mec-events', 'post');
      $meta_query = null;
    break;
  }

  $args = array(
    'post_type'     => $post_types,
    'post_per_page' => 10,
    'meta_query'    => $meta_query,
    'orderby'       => $date,
    'order'         => 'DESC',
  );
  $loop = new WP_Query( $args );
  $count = $loop->post_count;
  $post = $loop->post;
  ?>

  <section class="home-article">
    <div class="home-article__top">
      <div class="home-article__bg visible--on-tab">
        <img src="<?php bloginfo('template_url'); ?>/img/leo.svg" alt="Background Estate Fiorentina">
      </div>

      <div class="home-article__head txt-center">
        <?php if ( $acf_post_type == 'event' ) : ?>
          <?php if ( $count > 0 ) : ?>
            <a href="<?php bloginfo('url'); ?>/calendario/">
              <i class="mec-sl-calendar"></i>
            </a>
            <hgroup>
              <h3 class="home-article__title">
                <?php _e('Gli eventi di oggi', 'ae'); ?>
              </h3>
              <h2 class="home-article__date">
                <?php
                setlocale(LC_TIME,"it_IT");
                echo utf8_encode( strftime('%A %e %B %Y') );
                ?>
              </h2>
            </hgroup>
          <?php endif; ?>
        <?php elseif ( $acf_post_type == 'post' ) : ?>
          <h2 class="home-article__date">
            <?php _e('Le news di Estate Fiorentina 2019', 'ae'); ?>
          </h2>
        <?php else : ?>
          <h2 class="home-article__date">
            <?php _e('Scopri i prossimi eventi', 'ae'); ?>
          </h2>
        <?php endif; ?>
      </div>
    </div>

    <div class="home-article__wrap">
      <div class="row">
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <?php
          if($post->post_date_gmt >= $set_date) {
            $hide = '';
          } else {
            $hide = 'hide';
          }
          ?>

          <div class="row__column tab-12 desk-6 <?php echo $hide ?>">
            <?php get_template_part( 'template-parts/content', 'single-article' ); ?>
          </div>

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

      </div>

      <?php if ( $count < 10 ) : ?>
        <div class="home-article__head grid-container txt-center">
          <a href="<?php bloginfo('url'); ?>/calendario/">
            <i class="mec-sl-calendar"></i>
          </a>
          <hgroup>
            <h2 class="home-article__date">
              <?php _e('Gli eventi dei prossimi giorni', 'ae'); ?>
            </h2>
          </hgroup>
        </div>

        <?php
        $convertedTime = date('Y-m-d',strtotime('+15 day',strtotime($current_date)));
        // print_r($convertedTime)
        $args = array(
          'post_type'     => $post_types,
          'offset'        => $count,
          'post_per_page' => 10,
          'orderby'       => 'meta_value',
          'meta_key'      => $date,
          'meta_query'    => array(
            array(
              'key'       => $date,
              'value'     => $convertedTime,
              'compare'   => '<=',
            ),
            array(
              'key'       => $date,
              'value'     => $current_date,
              'compare'   => '>=',
            ),
          ),
          'order'         => 'ASC',
        );

        $loop = new WP_Query( $args );
        $post = $loop->post;
        ?>

        <div class="row">
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

            <?php
            $getPostCustom = get_post_custom();
            $event_date = $getPostCustom['mec_start_date'][0];

            // if($event_date >= $current_date && $event_date <= $convertedTime) {
            //   $hide = 'gino';
            // } else {
            //   $hide = 'hide';
            // }
            // if($post->post_date_gmt >= $set_date) {
            //   $hide = 'gino';
            // } else {
            //   $hide = 'test';
            // }
            ?>
            <div class="row__column tab-12 desk-6 <?php echo $hide ?>">
              <?php get_template_part( 'template-parts/content', 'single-article' ); ?>
            </div>

          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
        </div>
      <?php endif; ?>

    </div>


  </section>

</main><!-- #main -->
<?php get_footer(); ?>
