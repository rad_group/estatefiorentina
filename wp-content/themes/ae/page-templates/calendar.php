<?php
/**
 *
 * Template Name: Calendar
 *
 */

get_header();
?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <div class="filter-wrap">
    <?php get_template_part( 'template-parts/content', 'events-menu' ); ?>

    <?php
    while ( have_posts() ) : the_post();

      get_template_part( 'template-parts/content', 'page' );

    endwhile; // End of the loop.
    ?>
  </div>
</main><!-- #main -->
<?php get_footer(); ?>
