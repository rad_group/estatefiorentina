<?php
/**
 *
 * Template Name: Events
 *
 */

get_header();
?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <div class="filter-wrap">
    <?php get_template_part( 'template-parts/content', 'events-menu' ); ?>

    <?php $terms = get_terms('mec_category', array('post_type' => 'mec-events')); ?>

    <nav class="categories-nav">
      <div class="categories-nav__wrap">
        <ul class="categories-nav__list">
          <?php foreach ($terms as $term) : ?>
            <li class="categories-nav__list-single">
              <a class="category-label <?php echo $term->slug; ?>" href="<?php echo get_term_link($term->term_id); ?>" title="<?php echo $term->name; ?>"><?php echo $term->name; ?></a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </nav>
  </div>

  <div class="primary-wrap">
    <?php
    $current_date = date('Y-m-d');
    $convertedTime = date('Y-m-d',strtotime('+15 day',strtotime($current_date)));
    // print_r($convertedTime)

    ?>
    <?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $args = array(
      'post_type'     => 'mec-events',
      'post_per_page' => 10,
      'orderby'       => 'meta_value',
      'meta_key'      => 'mec_start_date',
      'date_query' => array(
        array(
            'year' => date('Y'),
        ),
      ),
      'meta-query' => array(
        array(
          'relation'    => 'OR',
          array(
            'key'          => 'mec_start_date',
            'value'        => $current_date,
            'compare'      => '<=',
          )
        )
      ),
      'order'         => 'ASC',
    );

    $loop = new WP_Query( $args );
    $post = $loop->post;
    $set_date = get_field('post_date', 6688);
    ?>

    <div class="row">
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <?php
        if($post->post_date_gmt >= $set_date) {
          $hide = '';
        } else {
          $hide = 'hide';
        }
        ?>

        <div class="row__column tab-12 desk-6 <?php //echo $hide; ?>">
          <?php get_template_part( 'template-parts/content', 'single-article' ); ?>
        </div>

      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>

    <div class="scroller-status">
      <div class="infinite-scroll-request loader-ellips"></div>
    </div>
    <p class="pagination">
      <?php next_posts_link('', $loop->max_num_pages); ?>
    </p>

  </div>


</main><!-- #main -->
<?php get_footer(); ?>
