<?php
/**
 *
 * Template Name: Contacts
 *
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <div class="single-contact__header-offset--contacts"></div>
  <header class="single-contact__header">
    <div class="grid-container--large">
      <h1 class="single-contact__header-title"><?php the_title(); ?></h1>
      <div class="single-contact__info">
        <h2>Puoi contattare lo Staff Comunicazione del sito Estate Fiorentina 2019 al seguente indirizzo email: <a href="mailto:comunicazione.ef@comune.fi.it" title="mail Estate Fiorentina">comunicazione.ef@comune.fi.it</a> </h2>
      </div>
    </div>
  </header>


  <section class="contact-form">

    <div class="grid-container--large">
      <div class="row">
        <div class="row__column tab-8">
          <div class="contact-form__flat">
            <?php echo do_shortcode('[contact-form-7 id="6687" title="Contact form 1"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

</main><!-- #main -->

<!-- <script>
  function initMap() {
    var location = {lat: 43.771714, lng: 11.260481};
    var iconBase = '?php bloginfo('template_directory') ?>/img/map-marker.svg';
    var map_style = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
];

    var infowindow = new google.maps.InfoWindow({
      content: 'Via Borgo Degli Albizi 12<br>50122 Firenze'
    });

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: location,
      draggable: true,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      scrollwheel: false,
      styles: map_style,
      disableDefaultUI: true,
    });

    var marker = new google.maps.Marker({
      position: location,
      animation: google.maps.Animation.DROP,
      map: map,
      //icon: iconBase,
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAp_EXqR7MiqlDEJ0ECKeDT6BrqvbCVSrw&callback=initMap">
</script> -->

<?php get_footer(); ?>
