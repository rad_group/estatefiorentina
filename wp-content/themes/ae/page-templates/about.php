<?php
/**
 *
 * Template Name: About
 *
 *
 */
get_header();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-page'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

  <div class="single-page__header-offset"></div>
  <header class="single-page__header">
    <div class="grid-container--large">
      <h1 class="single-page__header-title"><?php the_title(); ?></h1>
      <div class="page-subtitle">
        <h2> Estate Fiorentina 2019 è realizzata dal Comune di Firenze con il contributo di: </h2>
      </div>
    </div>
  </header>

  <div class="grid-container--large">
    <div class="single-page__main">
      <div class="row">

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_curatore')) { ?>
            <h2> Curatore </h2>
            <?php while(have_rows('lista_curatore')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_curatore_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_commissione_tecnico_artistica')) { ?>
            <h2> Commissione Tecnico/Artistica </h2>
            <?php while(have_rows('lista_commissione_tecnico_artistica')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_commissione_tecnico_artistica_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_direttore_cultura_sport')) { ?>
            <h2> Direttore Cultura e Sport </h2>
            <?php while(have_rows('lista_direttore_cultura_sport')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_direttore_cultura_sport_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_ufficio_eventi')) { ?>
            <h2> Ufficio Eventi </h2>
            <?php while(have_rows('lista_ufficio_eventi')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_ufficio_eventi_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_ufficio_comunicazione')) { ?>
            <h2> Ufficio Comunicazione </h2>
            <?php while(have_rows('lista_ufficio_comunicazione')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_ufficio_comunicazione_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_ufficio_gabinetto')) { ?>
            <h2> Ufficio Comunicazione </h2>
            <?php while(have_rows('lista_ufficio_gabinetto')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_ufficio_gabinetto_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_ufficio_stampa')) { ?>
            <h2> Ufficio Stampa </h2>
            <?php while(have_rows('lista_ufficio_stampa')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_ufficio_stampa_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column mobile-6 tab-6 lista">
          <?php if(have_rows('lista_illustratrice')) { ?>
            <h2> Ufficio Comunicazione </h2>
            <?php while(have_rows('lista_illustratrice')) { the_row(); ?>
            <p> <?php echo get_sub_field('lista_illustratrice_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

        <div class="row__column tab-12 si-ringraziano">
          <?php if(have_rows('si_ringraziano')) { ?>
            <h2> Si ringraziano </h2>
            <?php while(have_rows('si_ringraziano')) { the_row(); ?>
            <p> <?php echo get_sub_field('si_ringraziano_name') ?> </p>
            <?php } ?>
          <?php } ?>
        </div>

      </div>

    </div>
    <div class="single-page__content">
      <h2> Lista delle associazioni in ordine alfabetico </h2>
      <?php
      while ( have_posts() ) : the_post();
        the_content();
      endwhile; // End of the loop.
      ?>
    </div><!-- .single-post__content -->
  </div>

</article><!-- #post-## -->

<?php get_footer(); ?>
