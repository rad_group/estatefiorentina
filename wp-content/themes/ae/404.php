<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--large">
  <div class="row">
    <div class="row__column">
      <main id="main" class="site-main" role="main">

        <section class="error-404 not-found">
          <header class="page-header">
            <h1 class="page-title">
              <?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'ae' ); ?>
            </h1>
          </header><!-- .page-header -->

          <div class="page-content">
            <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'ae' ); ?></p>

            <?php get_search_form(); ?>

          </div><!-- .page-content -->
        </section><!-- .error-404 -->

      </main><!-- #main -->
    </div>

  </div> <!-- .row -->
</div>
<?php get_footer(); ?>
