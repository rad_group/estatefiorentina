<?php
	/*
		Single Social Share Template Part
	*/

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

?>

<ul class="single-share-icons">

	<?php

		$huzi_twitter_url_params = array(
		    'url'	=> esc_url( get_the_permalink() ),
		    'text'	=> esc_html( get_the_title() )
		);

		$huzi_twitter_url = esc_url( "https://twitter.com/share?" . http_build_query( $huzi_twitter_url_params ) );

	?>

	<li><a class="twitter social-pop" title="<?php esc_attr_e( 'Share On Twitter', 'huzi' ) ?>" target="_blank" href="<?php echo esc_url( $huzi_twitter_url ); ?>"><i class='fa fa-twitter' aria-hidden='true'></i><span><?php esc_html_e( 'Share On Twitter', 'huzi' ) ?></span></a></li>

	<?php

		$huzi_facebook_url_params = array(
		    'u'	=> esc_url( get_the_permalink() )
		);

		$huzi_facebook_url = esc_url( "https://www.facebook.com/sharer.php?" . http_build_query( $huzi_facebook_url_params ) );

	?>

	<li><a class="facebook social-pop" title="<?php esc_attr_e( 'Share On Facebook', 'huzi' ) ?>" target="_blank" href="<?php echo esc_url( $huzi_facebook_url ); ?>"><i class='fa fa-facebook' aria-hidden='true'></i><span><?php esc_html_e( 'Share On Facebook', 'huzi' ) ?></span></a></li>

	<?php

		$huzi_googleplus_url_params = array(
		    'url'	=> esc_url( get_the_permalink() )
		);

		$huzi_googleplus_url = esc_url( "https://plus.google.com/share?" . http_build_query( $huzi_googleplus_url_params ) );

	?>

	<li><a class="google social-pop" title="<?php esc_attr_e( 'Share On Google+', 'huzi' ) ?>" target="_blank" href="<?php echo esc_url( $huzi_googleplus_url ); ?>"><i class='fa fa-google-plus' aria-hidden='true'></i></a></li>

	<?php

		$huzi_image_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );

		$huzi_pinterest_url_params = array(
		    'url'	=> esc_url( get_the_permalink() ),
		    'media'	=> esc_url( $huzi_image_url ),
		    'description'	=> esc_html( get_the_title() )
		);

		$huzi_pinterest_url = esc_url( "https://pinterest.com/pin/create/button/?" . http_build_query( $huzi_pinterest_url_params ) );

	?>

	<li><a class="pinterest social-pop" title="<?php esc_attr_e( 'Pin it!', 'huzi' ) ?>" target="_blank" href="<?php echo esc_url( $huzi_pinterest_url ); ?>"><i class='fa fa-pinterest-p' aria-hidden='true'></i></a></li>

	<li><a class="mail-to" title="<?php esc_attr_e( 'Share via Email', 'huzi' ) ?>" href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>"><i class='fa fa-envelope' aria-hidden='true'></i></a></li>
	
</ul>