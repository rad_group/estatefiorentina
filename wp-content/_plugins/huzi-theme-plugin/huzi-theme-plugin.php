<?php
/**
* Plugin Name: Huzi Theme Plugin
* Plugin URI: http://themeforest.net/user/promola
* Description: This plugin adds widgets and other theme specific features to your site.
* Version: 1.0.1
* Author: Promola
* Author URI: http://promola.co.za
* License: Located in the 'Licensing' folder
*/

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
	}

	/*------------------------------------------
		Adds the Promola Mega Menu
	------------------------------------------*/

	add_filter( 'wp_nav_menu_objects', 'huzi_add_images_to_special_submenu' );

	function huzi_add_images_to_special_submenu( $items ) {
	    $huzi_mega_menu_parent_ids = array();

	    foreach ( $items as $item ) {
	        if ( in_array( 'huzimega', $item->classes, true ) && isset( $item->ID ) ) {
	            $huzi_mega_menu_parent_ids[] = $item->ID;
	        }

	        if ( in_array( $item->menu_item_parent, $huzi_mega_menu_parent_ids ) && has_post_thumbnail( $item->object_id ) ) {
	            $item->title = sprintf(
	                '%1$s %2$s %3$s %4$s',
	                '<span class="post-thumb">' . get_the_post_thumbnail( $item->object_id, 'huzi-mega-menu-thumb', array( 'alt' => esc_attr( $item->title ) ) ) . '</span>',
	                '<span class="' . get_post_format( $item->object_id ). '-post-icon format-icon"></span>',
	                '<span class="mega-title">' . $item->title . '</span>',
	                '<span class="mega-date"><span>'. esc_html__('On: ', 'huzi') . '</span>'. get_the_time( get_option('date_format'), $item->object_id ) . '</span>'
	            );
	        }
	    }

	    return $items;
	}

	/*------------------------------------------
		Adds the Posts View Count
	------------------------------------------*/

	function huzi_set_post_views($postID) {

	    $count_key = 'huzi_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);

	    if($count==''){

	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');

	    } else {
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }

	}
	//To keep the count accurate, lets get rid of prefetching
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

	function huzi_track_post_views ($post_id) {

	    if ( !is_single() ) return;

	    if ( empty ( $post_id) ) {
	        global $post;
	        $post_id = $post->ID;    
	    }
	    huzi_set_post_views($post_id);

	}
	add_action( 'wp_head', 'huzi_track_post_views');

	function huzi_get_post_views($postID) {

	    $count_key = 'huzi_post_views_count';

	    $count = get_post_meta($postID, $count_key, true);

	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0";
	    }

	    return $count;
	}

	/*------------------------------------------
		Adds the Promola Popular Posts widget
	------------------------------------------*/

	class huzi_Popular_Widget extends WP_Widget {
	
		function __construct()
		{
			$huzi_params = array(
				'description' => esc_html__('Display most commented posts with thumbnails.', 'huzi'),
				'name' => esc_html__('Promola Popular Posts', 'huzi')
			);
			parent::__construct('huzi_popular', '', $huzi_params);
		}
		
		public function form($instance)
		{
			extract($instance);
			?>
				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="text"
						id="<?php echo esc_attr( $this->get_field_id('title') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('title') ); ?>"
						value="<?php if(isset($title)) echo esc_attr($title); ?>">
				</p>
				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('number') ); ?>"><?php esc_html_e('Number:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="number"
						id="<?php echo esc_attr( $this->get_field_id('number') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('number') ); ?>"
						value="<?php if(isset($number)) echo esc_attr($number); else echo "3"; ?>">
				</p>
				
			<?php
		}
		
		public function widget($args, $instance)
		{
			extract($args);
			extract($instance);
			
			global $post;
			$args = array(
				'numberposts' => $number,
				'meta_key' => 'huzi_post_views_count',
				'orderby' => 'meta_value_num'
			);
				
			$custom_posts = get_posts($args);
			
			echo $before_widget;
			
				if(!empty($title))
				{
					echo $before_title . $title . $after_title;
				} ?>

				<?php foreach($custom_posts as $post) : setup_postdata($post); ?>
					
					<?php get_template_part('template-parts/posts/small', 'post'); ?>
					
				<?php endforeach; ?>

				<?php wp_reset_postdata(); ?>
				
			<?php echo $after_widget;
		}
		
	}

	add_action('widgets_init', 'huzi_reg_popular_widget');

	function huzi_reg_popular_widget()
	{
		register_widget('huzi_Popular_Widget');
	}


	/*------------------------------------------
		Adds the Promola Recent Posts widget
	------------------------------------------*/

	class huzi_Recent_Widget extends WP_Widget {
	
		function __construct()
		{
			$huzi_params = array(
				'description' => esc_html__('Display recent posts with thumbnails.', 'huzi'),
				'name' => esc_html__('Promola Recent Posts', 'huzi')
			);
			parent::__construct('huzi_recent', '', $huzi_params);
		}
		
		public function form($instance)
		{
			extract($instance);
			?>
				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="text"
						id="<?php echo esc_attr( $this->get_field_id('title') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('title') ); ?>"
						value="<?php if(isset($title)) echo esc_attr($title); ?>">
				</p>
				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('number') ); ?>"><?php esc_html_e('Number:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="number"
						id="<?php echo esc_attr( $this->get_field_id('number') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('number') ); ?>"
						value="<?php if(isset($number)) echo esc_attr($number); else echo "3"; ?>">
				</p>
				
			<?php
		}
		
		public function widget($args, $instance)
		{
			extract($args);
			extract($instance);
			
			global $post;
			$args = array(
				'numberposts' => $number
			);
				
			$custom_posts = get_posts($args);
			
			echo $before_widget;
			
				if(!empty($title))
				{
					echo $before_title . $title . $after_title;
				} ?>

				<?php foreach($custom_posts as $post) : setup_postdata($post); ?>
					
					<?php get_template_part('template-parts/posts/small', 'post'); ?>
					
				<?php endforeach; ?>

				<?php wp_reset_postdata(); ?>
				
			<?php echo $after_widget;
		}
		
	}

	add_action('widgets_init', 'huzi_reg_recent_widget');

	function huzi_reg_recent_widget()
	{
		register_widget('huzi_Recent_Widget');
	}


	/*------------------------------------------
		Adds the Promola Social Media widget
	------------------------------------------*/

	class huzi_Social_Count_Widget extends WP_Widget {
		
		function __construct()
		{
			$params = array(
				'description' => esc_html__('Social media widget.', 'huzi'),
				'name' => esc_html__('Promola Social Media', 'huzi')
			);
			parent::__construct('huzi_social_count', '', $params);
		}
		
		public function form($instance)
		{
			extract($instance);
			?>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="text"
						id="<?php echo esc_attr( $this->get_field_id('title') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('title') ); ?>"
						value="<?php if(isset($title)) echo esc_attr($title); ?>">
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('twitter_url') ); ?>"><?php esc_html_e('Twitter Profile Link:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="url"
						id="<?php echo esc_attr( $this->get_field_id('twitter_url') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('twitter_url') ); ?>"
						value="<?php if(isset($twitter_url)) echo esc_attr($twitter_url); ?>">
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('facebook_url') ); ?>"><?php esc_html_e('Facebook Page Link:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="url"
						id="<?php echo esc_attr( $this->get_field_id('facebook_url') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('facebook_url') ); ?>"
						value="<?php if(isset($facebook_url)) echo esc_attr($facebook_url); ?>">
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('instagram_url') ); ?>"><?php esc_html_e('Instagram Profile Link:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="url"
						id="<?php echo esc_attr( $this->get_field_id('instagram_url') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('instagram_url') ); ?>"
						value="<?php if(isset($instagram_url)) echo esc_attr($instagram_url); ?>">
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('youtube_url') ); ?>"><?php esc_html_e('YouTube Profile Link:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="url"
						id="<?php echo esc_attr( $this->get_field_id('youtube_url') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('youtube_url') ); ?>"
						value="<?php if(isset($youtube_url)) echo esc_attr($youtube_url); ?>">
				</p>
				
			<?php
		}
		
		public function widget($args, $instance)
		{
			extract($args);
			extract($instance);
			
			echo $before_widget;

				if( !empty($title) ) {

					echo $before_title . $title . $after_title;

				}

				echo "<ul class='social-widget-list'>";

						if (!empty($twitter_url)) {

							echo "<li class='twitter'>";

							echo "<a href=' ". esc_url( $twitter_url ) ." ' target='_blank'><span class='wd-icon-wrap'><i class='fa fa-twitter' aria-hidden='true'></i></span><span class='wd-social-text'>". esc_html__('Follow', 'huzi') . "</span></a>";

							echo "</li>";
						}

						if (!empty($facebook_url)) {

							echo "<li class='facebook'>";

							echo "<a href=' ". esc_url( $facebook_url ) ." ' target='_blank'><span class='wd-icon-wrap'><i class='fa fa-facebook' aria-hidden='true'></i></span><span class='wd-social-text'>". esc_html__('Like', 'huzi') . "</span></a>";

							echo "</li>";
						
						}

						if (!empty($instagram_url)) {

							echo "<li class='instagram'>";

							echo "<a href=' ". esc_url( $instagram_url ) ." ' target='_blank'><span class='wd-icon-wrap'><i class='fa fa-instagram' aria-hidden='true'></i></span><span class='wd-social-text'>". esc_html__('Follow', 'huzi') . "</span></a>";

							echo "</li>";
						}

						if (!empty($youtube_url)) {

							echo "<li class='youtube'>";

							echo "<a href=' ". esc_url( $youtube_url ) ." ' target='_blank'><span class='wd-icon-wrap'><i class='fa fa-youtube' aria-hidden='true'></i></span><span class='wd-social-text'>". esc_html__('Subscribe', 'huzi') . "</span></a>";

							echo "</li>";
						}

				echo "</ul>";

			echo $after_widget;
		}
		
	}

	add_action('widgets_init', 'huzi_reg_count_widget');

	function huzi_reg_count_widget()
	{
		register_widget('huzi_Social_Count_Widget');
	}

	/*------------------------------------------
		Adds the Promola About widget
	------------------------------------------*/

	class huzi_About_Widget extends WP_Widget {
		
		function __construct()
		{
			$params = array(
				'description' => esc_html__('A simple about us widget.', 'huzi'),
				'name' => esc_html__('Promola About Us', 'huzi')
			);
			parent::__construct('huzi_about', '', $params);
		}
		
		public function form($instance)
		{
			extract($instance);
			?>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title', 'huzi'); ?>:</label>
					<input
						class="widefat"
						type="text"
						id="<?php echo esc_attr( $this->get_field_id('title') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('title') ); ?>"
						value="<?php if(isset($title)) echo esc_attr($title); ?>">
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('logo_url') ); ?>"><?php esc_html_e('Link To Logo Or Photo:', 'huzi'); ?></label>
					<input
						class="widefat"
						type="url"
						id="<?php echo esc_attr( $this->get_field_id('logo_url') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('logo_url') ); ?>"
						value="<?php if(isset($logo_url)) echo esc_attr($logo_url); ?>">
				</p>

				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('abouttxt') ); ?>"><?php esc_html_e('About Company Or Author', 'huzi'); ?>:</label>
					<textarea
						class="widefat"
						rows="10"
						id="<?php echo esc_attr( $this->get_field_id('abouttxt') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('abouttxt') ); ?>"><?php if(isset($abouttxt)) echo esc_html($abouttxt); ?></textarea>
				</p>
				
			<?php
		}
		
		public function widget($args, $instance)
		{
			extract($args);
			extract($instance);
			
			echo $before_widget;

				if( !empty($title) ) {

					echo $before_title . $title . $after_title;

				}

				echo "<div class='about-widget-wrap'>";

					if (!empty($logo_url)) {

						echo "<span class='about-img'><img src=' " . esc_url( $logo_url ) . " ' ></span>";

					}
					
					echo wpautop( $abouttxt );

					echo "<ul class='about-widget-social'>";

						if( esc_html( of_get_option('twitter_url') ) ) {

							echo "<li><a href='". esc_url( of_get_option('twitter_url') ) ."' title='". esc_attr__('Twitter', 'huzi') ."' target='_blank' class='twitter'><i class='fa fa-twitter' aria-hidden='true'></i></a></li>";
							
						}

						if( esc_html( of_get_option('facebook_url') ) ) {

							echo "<li><a href='". esc_url( of_get_option('facebook_url') ) ."' title='". esc_attr__('Facebook', 'huzi') ."' target='_blank' class='facebook'><i class='fa fa-facebook' aria-hidden='true'></i></a></li>";

						}

						if( esc_html( of_get_option('instagram_url') ) ) {

							echo "<li><a href='". esc_url( of_get_option('instagram_url') ) ."' title='". esc_attr__('Instagram', 'huzi') ."' target='_blank' class='instagram'><i class='fa fa-instagram' aria-hidden='true'></i></a></li>";
							
						}

						if( esc_html( of_get_option('google_url') ) ) {

							echo "<li><a href='". esc_url( of_get_option('google_url') ) ."' title='". esc_attr__('Google Plus', 'huzi') ."' target='_blank' class='google'><i class='fa fa-google-plus' aria-hidden='true'></i></a></li>";
							
						}

						if( esc_html( of_get_option('youtube_url') ) ) {

							echo "<li><a href='". esc_url( of_get_option('youtube_url') ) ."' title='". esc_attr__('YouTube', 'huzi') ."' target='_blank' class='youtube'><i class='fa fa-youtube' aria-hidden='true'></i></a></li>";
							
						}
							
					echo "</ul>";

				echo "</div>";

			echo $after_widget;
		}
		
	}

	add_action('widgets_init', 'huzi_reg_about_widget');

	function huzi_reg_about_widget()
	{
		register_widget('huzi_About_Widget');
	}


	/*------------------------------------------
		Adds the Promola Videos widget
	------------------------------------------*/

	class huzi_Video_Widget extends WP_Widget {
	
		function __construct()
		{
			$huzi_params = array(
				'description' => esc_html__('Display YouTube and Vimeo videos in the sidebar and footer.', 'huzi'),
				'name' => esc_html__('Promola Videos', 'huzi')
			);
			parent::__construct('molavid', '', $huzi_params);
		}
		
		public function form($instance)
		{
			extract($instance);
			?>
				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title', 'huzi'); ?>:</label>
					<input
						class="widefat"
						type="text"
						id="<?php echo esc_attr( $this->get_field_id('title') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('title') ); ?>"
						value="<?php if(isset($title)) echo esc_attr($title); ?>">
				</p>
				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id('videourl') ); ?>"><?php esc_html_e('Video Embed', 'huzi'); ?>:</label>
					<textarea
						class="widefat"
						rows="10"
						id="<?php echo esc_attr( $this->get_field_id('videourl') ); ?>"
						name="<?php echo esc_attr( $this->get_field_name('videourl') ); ?>"><?php if(isset($videourl)) echo esc_html($videourl); ?></textarea>
				</p>
				
			<?php
		}
		
		public function widget($args, $instance)
		{
			extract($args);
			extract($instance);
			
			echo $before_widget;
			
				if(!empty($title))
				{
					echo $before_title . $title . $after_title;
				}

				$huzi_allowed_attr = array(
					'iframe' => array(
						'src'             => array(),
						'height'          => array(),
						'width'           => array(),
						'frameborder'     => array(),
						'allowfullscreen' => array()
					), 
				);

				echo '<div class="video-widget-wrap">';
				
				echo wp_kses( $videourl, $huzi_allowed_attr );

				echo '</div>';
				
			echo $after_widget;
		}
		
	}

	add_action('widgets_init', 'huzi_reg_video_widget');

	function huzi_reg_video_widget()
	{
		register_widget('huzi_Video_Widget');
	}


	/*------------------------------------------
		Adds the Facebook Like Box widget
	------------------------------------------*/

	/*
    Original Widget was by Paul Underwood released under the GNU license
	*/

	/**
	 * Register the Widget
	 */
	add_action( 'widgets_init', create_function( '', 'register_widget("huzi_facebook_widget");' ) ); 

	/**
	 * Create the widget class and extend from the WP_Widget
	 */
	 class huzi_facebook_widget extends WP_Widget {
	 	
		/**
		 * Set the widget defaults
		 */
		private $widget_title = "Facebook Page";
		private $facebook_id = "244325275637598";
		private $facebook_username = "promolathemes";
	 	
		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			
			parent::__construct(
				'huzi_facebook_widget',		// Base ID
				'Facebook Like Box',		// Name
				array(
					'classname'		=>	'huzi_facebook_widget',
					'description'	=>	esc_html__('A widget that displays a facebook like box from your facebook page.', 'huzi')
				)
			);

		} // end constructor
		
		/**
		 * Add Facebook javascripts
		 */
		public function huzi_add_js(){
			echo '<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&version=v2.3&appId='. esc_js( $this->facebook_id ).'";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, \'script\', \'facebook-jssdk\'));</script>';
		}

		
		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		public function widget( $args, $instance ) {
			extract( $args );

			/* Our variables from the widget settings. */
			$this->widget_title = apply_filters('widget_title', $instance['title'] );
			
			$this->facebook_id = $instance['app_id'];
			$this->facebook_username = $instance['page_name'];
			
			add_action('wp_footer', array(&$this,'huzi_add_js'));

			/* Before widget (defined by themes). */
			echo $before_widget;

			/* Display the widget title if one was input (before and after defined by themes). */
			if ( $this->widget_title )
				echo $before_title . $this->widget_title . $after_title;

			/* Like Box */
			 ?>

				<div class="fb-page" 
					data-href="https://www.facebook.com/<?php echo esc_js( $this->facebook_username ); ?>"
					data-width="372" 
					data-adapt-container-width="true" 
					data-hide-cover="false" 
					data-show-facepile="true" 
					data-show-posts="true">
					<div class="fb-xfbml-parse-ignore">
						<blockquote cite="https://www.facebook.com/<?php echo esc_js( $this->facebook_username ); ?>">
							<a href="https://www.facebook.com/<?php echo esc_js( $this->facebook_username ); ?>"><?php echo esc_js( $this->facebook_username ); ?></a>
						</blockquote>
					</div>
				</div>

			<?php 

			/* After widget (defined by themes). */
			echo $after_widget;
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param array $new_instance Values just sent to be saved.
		 * @param array $old_instance Previously saved values from database.
		 *
		 * @return array Updated safe values to be saved.
		 */
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['app_id'] = strip_tags( $new_instance['app_id'] );
			$instance['page_name'] = strip_tags( $new_instance['page_name'] );

			return $instance;
		}
		
		/**
		 * Create the form for the Widget admin
		 *
		 * @see WP_Widget::form()
		 *
		 * @param array $instance Previously saved values from database.
		 */	 
		function form( $instance ) {

			/* Set up some default widget settings. */
			$defaults = array(
			'title' => $this->widget_title,
			'app_id' => $this->facebook_id,
			'page_name' => $this->facebook_username,
			);
			
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>

			<!-- Widget Title: Text Input -->
			<p>


				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e('Title:', 'huzi') ?></label>
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
			</p>

			<!-- App id: Text Input -->
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'app_id' ) ); ?>"><?php esc_html_e('App Id', 'huzi') ?></label>
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'app_id' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'app_id' ) ); ?>" value="<?php echo esc_attr( $instance['app_id'] ); ?>" />
			</p>
			
			<!-- Page name: Text Input -->
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'page_name' ) ); ?>"><?php esc_html_e('Page name (http://www.facebook.com/[page_name])', 'huzi') ?></label>
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'page_name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'page_name' ) ); ?>" value="<?php echo esc_attr( $instance['page_name'] ); ?>" />
			</p>
			
		<?php
		}
	}


	/*------------------------------------------------------------------
		Adds social media fields on "Your Profile" page in the admin
	------------------------------------------------------------------*/

    function huzi_author_contact( $contactmethods ) {
        
        $contactmethods[ 'twitter' ] = 'Twitter';
        $contactmethods[ 'facebook' ] = 'Facebook';
        $contactmethods[ 'google' ] = 'Google Plus';
        $contactmethods[ 'instagram' ] = 'Instagram';
        $contactmethods[ 'linkedin' ] = 'LinkedIn';
     
        return $contactmethods;
    }
    add_filter( 'user_contactmethods', 'huzi_author_contact' );

    /*------------------------------------------
		Enable Ajax Load More Posts
	------------------------------------------*/

	add_action('wp_ajax_nopriv_huzi_load_more', 'huzi_load_more');
	add_action('wp_ajax_huzi_load_more', 'huzi_load_more');

	function huzi_load_more() {

		$paged = $_POST["page"]+1;

		$huzi_number_of_posts = of_get_option('recent_posts');

		$temp = $wp_query;
		$wp_query= null;
		$wp_query = new WP_Query();

		$wp_query->query('posts_per_page='.$huzi_number_of_posts.'&paged='.$paged.'&post_status=publish&post_type=post');

		while ($wp_query->have_posts()) : $wp_query->the_post();

			get_template_part('template-parts/posts/medium', 'post');

		endwhile;

		wp_reset_postdata();

		die();

	}

	 /*------------------------------------------
		Post Rating Meta Box
	------------------------------------------*/

	function huzi_custom_meta() {
        add_meta_box( 'huzi_meta', esc_html__( 'Post Rating', 'huzi' ), 'huzi_meta_callback', 'post', 'side', 'low' );
    }
    add_action( 'add_meta_boxes', 'huzi_custom_meta' );

    function huzi_meta_callback( $post ) {
        wp_nonce_field( basename( __FILE__ ), 'huzi_nonce' );
        $huzi_stored_meta = get_post_meta( $post->ID );
        ?>
     
        <p class="howto">
            <label for="meta-rating"><?php esc_attr_e( 'Enter your rating', 'huzi' )?></label>
        </p>
        <p>
            <input type="text" name="meta-rating" id="meta-rating" class="widefat" value="<?php if ( isset ( $huzi_stored_meta['meta-rating'] ) ) echo esc_html( $huzi_stored_meta['meta-rating'][0] ); ?>">
        </p>
     
        <?php
    }

    function huzi_meta_save( $post_id ) {
 
        // Checks save status
        $huzi_is_autosave = wp_is_post_autosave( $post_id );
        $huzi_is_revision = wp_is_post_revision( $post_id );
        $huzi_is_valid_nonce = ( isset( $_POST[ 'huzi_nonce' ] ) && wp_verify_nonce( $_POST[ 'huzi_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     
        // Exits script depending on save status
        if ( $huzi_is_autosave || $huzi_is_revision || !$huzi_is_valid_nonce ) {
            return;
        }
     
        // Checks for input and sanitizes/saves if needed
        if( isset( $_POST[ 'meta-rating' ] ) ) {
            update_post_meta( $post_id, 'meta-rating', sanitize_text_field( $_POST[ 'meta-rating' ] ) );
        }
     
    }
    add_action( 'save_post', 'huzi_meta_save' );

    /*------------------------------------------
		Adds the Promola Share Shortcodes
	------------------------------------------*/

	// Header Share
	function huzi_header_share(){

		ob_start();

		include( plugin_dir_path( __FILE__ ) . 'header-social-share.php' );

		$output = ob_get_contents();

		ob_end_clean();

		return $output;

	}
	add_shortcode( 'huzi-header-share', 'huzi_header_share' );

	// Posts Share
	function huzi_social_share(){

		ob_start();

		include( plugin_dir_path( __FILE__ ) . 'social-share.php' );

		$output = ob_get_contents();

		ob_end_clean();

		return $output;

	}
	add_shortcode( 'huzi-social', 'huzi_social_share' );

	// Single Post Page Share
	function huzi_single_share(){

		ob_start();

		include( plugin_dir_path( __FILE__ ) . 'single-social-share.php' );

		$output = ob_get_contents();

		ob_end_clean();

		return $output;

	}
	add_shortcode( 'huzi-single-share', 'huzi_single_share' );

?>
